import React from "react";

export const Autocomplete = ({ value, options, renderInput, renderOption, onInputChange }) => {
  return (
    <div className="auto-complete" data-testid="auto-complete">
      <div className="auto-complete__input-container">{renderInput({ value, onChange: onInputChange })}</div>
      <div className="auto-complete__result-container">
        {options.map((option, idx) => (
          <React.Fragment key={idx}>{renderOption(option, { inputValue: value })}</React.Fragment>
        ))}
      </div>
    </div>
  );
};

export default Autocomplete;
