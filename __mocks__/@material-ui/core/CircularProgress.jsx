import React from "react";

const CircularProgress = () => <div data-testid="circular-progress"></div>;

export default CircularProgress;
