const path = require("path");
const webpack = require("webpack");
const { merge } = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlMinimizerPlugin = require("html-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const fs = require("fs");

const prodPackage = process.env.NODE_ENV === "prod_package" ? true : false;
const devPackage = process.env.NODE_ENV === "dev_package" ? true : false;
const devTestpage = process.env.NODE_ENV === "dev_testpage" ? true : false;

// ------------------------------------------------------------------------------------------------- package banner ----

const package = require("./package.json");
const packageBanner = package.name + "\n" + "@version: " + package.version + "\n" + "@license: " + package.license;

// ------------------------------------------------------------------------------------------- third party licenses ----

const thirdPartyPackages = {
  "@babel/plugin-transform-runtime": {
    location: path.resolve(__dirname, "./node_modules/@babel/plugin-transform-runtime/LICENSE")
  },
  "core-js": {
    location: path.resolve(__dirname, "./node_modules/core-js/LICENSE")
  },
  "hyphenate-style-name": {
    location: path.resolve(__dirname, "./node_modules/hyphenate-style-name/LICENSE")
  }
};

const thirdPartyLicenses = Object.keys(thirdPartyPackages).reduce((licenses, packageName) => {
  try {
    const data = fs.readFileSync(thirdPartyPackages[packageName].location, "utf8");

    const updatedLicenses =
      licenses +
      "==================================================================================" +
      "\n\n" +
      packageName +
      "\n" +
      "@license: " +
      "\n" +
      data +
      "\n\n";

    return updatedLicenses;
  } catch (err) {
    console.error("error reading file:", err);
    return licenses;
  }
}, "");

fs.writeFile("thirdparty-licenses.txt", thirdPartyLicenses, (err, result) => {
  if (err) console.log("error", err);
});

// -------------------------------------------------------------------------------------------- webpack base config ----

const baseConfig = {
  context: __dirname,
  node: {
    setImmediate: false
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.BannerPlugin(packageBanner),
    new CompressionPlugin({ test: /\.(js|css|svg)$/ })
  ],
  resolve: {
    extensions: [".js", ".es6", ".json", ".jsx"],
    modules: [path.resolve("./node_modules"), path.resolve("./src")]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: {
          test: /node_modules/,
          not: [/(promise-controller)/]
        },
        use: [
          {
            loader: "babel-loader"
          }
        ]
      },
      {
        test: /\.(js)$/,
        loader: "babel-loader",
        include: __dirname + "/src"
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        loader: { loader: "text-loader" }
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(sass|scss)$/,
        exclude: /node_modules/,
        use: ["style-loader", "css-loader", "sass-loader"]
      }
    ]
  }
};

// --------------------------------------------------------------------------------------------- webpack dev config ----

const devTestpageConfig = {
  mode: "development",
  devtool: "eval-source-map",
  entry: "./src/index.js",
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "./build"),
    sourceMapFilename: "[file].map.js",
    globalObject: `(typeof self !== 'undefined' ? self : this)`
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      scriptLoading: 'defer',
      template: path.join(__dirname, "src", "index.html"),
      filename: "index.html"
    })
  ]
};

const devConfig = {
  mode: "development",
  devtool: "eval-cheap-source-map",
  entry: "./src/knowledge.js",
  output: {
    filename: "messenger-knowledge-components.min.js",
    path: path.resolve(__dirname, "./build"),
    sourceMapFilename: "[file].map.js",
    libraryTarget: "commonjs2",
    globalObject: `(typeof self !== 'undefined' ? self : this)`
  },
  plugins: []
};

// -------------------------------------------------------------------------------------------- webpack prod config ----

const prodConfig = {
  mode: "production",
  devtool: "none",
  entry: "./src/knowledge.js",
  output: {
    filename: "messenger-knowledge-components.min.js",
    path: path.resolve(__dirname, "./build"),
    sourceMapFilename: "[file].map.js",
    libraryTarget: "commonjs2",
    globalObject: `(typeof self !== 'undefined' ? self : this)`
  },
  plugins: [],
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
      terserOptions: {
        format: {
          comments: false,
        },
        compress: {
          drop_console: true,
        }
      },
      extractComments: false,
    }), new HtmlMinimizerPlugin()]
  },
};

// ----------------------------------------------------------------------------------------- webpack prod externals ----

// This configuration is required to exclude dependencies which will be provided by the messenger codebase!!

// Reference an alias to any new packages here and use this name in the externals!
// This will exlcude that package from including it in the build.

const npmExternals = {
  resolve: {
    alias: {
      react: path.resolve(__dirname, "./node_modules/react"),
      "react-dom": path.resolve(__dirname, "./node_modules/react-dom"),
      "@material-ui/core": path.resolve(__dirname, "./node_modules/@material-ui/core"),
      "@material-ui/lab": path.resolve(__dirname, "./node_modules/@material-ui/lab"),
      "@material-ui/styles": path.resolve(__dirname, "./node_modules/@material-ui/styles")
    },
    extensions: [".js", ".es6", ".json", "jsx"],
    modules: [path.resolve("./node_modules"), path.resolve("./src")]
  },
  externals: {
    react: {
      commonjs: "react",
      commonjs2: "react",
      amd: "react",
      root: "react"
    },
    "react-dom": {
      commonjs: "react-dom",
      commonjs2: "react-dom",
      amd: "ReactDOM",
      root: "ReactDOM"
    },
    "prop-types": {
      commonjs: "prop-types",
      commonjs2: "prop-types",
      amd: "prop-types",
      root: "prop-types"
    },
    "@material-ui/core": {
      root: "@material-ui/core",
      commonjs2: "@material-ui/core",
      commonjs: "@material-ui/core",
      amd: "@material-ui/core"
    },
    "@material-ui/lab": {
      root: "@material-ui/lab",
      commonjs2: "@material-ui/lab",
      commonjs: "@material-ui/lab",
      amd: "@material-ui/lab"
    },
    "@material-ui/styles": {
      root: "@material-ui/styles",
      commonjs2: "@material-ui/styles",
      commonjs: "@material-ui/styles",
      amd: "@material-ui/styles"
    }
  }
};

// -------------------------------------------------------------------------------------------------------- exports ----

module.exports = prodPackage
  ? merge(baseConfig, prodConfig, npmExternals)
  : devPackage
  ? merge(baseConfig, devConfig, npmExternals)
  : merge(baseConfig, devTestpageConfig);
