import { parseArticleRaw } from "./knowledge.parser.js";

const articleToParse = JSON.parse(`{
    "blocks": [
      {
        "type": "Paragraph",
        "paragraph": {
          "blocks": [
            {"type":"Text","text":{"text":"Mozzarella, salami and tuna.This text contains ","marks":null,"hyperlink":null}},
            {"type":"Text","text":{"text":"text in bold","marks":["Bold"],"hyperlink":null}},
            {"type":"Text","text":{"text":" and ","marks":null,"hyperlink":null}},
            {"type":"Text","text":{"text":"italics","marks":["Italic"],"hyperlink":null}},
            {"type":"Text","text":{"text":" as well as a ","marks":["Underline"],"hyperlink":null}},
            {"type":"Text","text":{"text":"hyperlink","marks":null,"hyperlink":"https://en.wikipedia.org/wiki/Hypertext"}},
            {"type":"Text","text":{"text":".","marks":null,"hyperlink":null}}
          ]
        },
        "image": null,
        "list": null
      }
    ]
}`);

describe("test article parsing from json payload", () => {
  test("valid payload should be parsed", () => {
    const actual = parseArticleRaw(articleToParse);
    // eslint-disable-next-line prettier/prettier
    const expected = '<p>Mozzarella, salami and tuna.This text contains <b>text in bold</b> and <i>italics</i><u> as well as a </u><a href="https://en.wikipedia.org/wiki/Hypertext" target="_blank" rel="noreferrer">hyperlink</a>.</p>';     
    expect(actual).toBe(expected);
  });
});
