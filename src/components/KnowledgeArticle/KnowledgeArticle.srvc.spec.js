import httpClient from "components/~common/httpClient.utils";
import { getArticle } from "./KnowledgeArticle.srvc";

jest.mock("components/~common/httpClient.utils");
jest.mock("~common/utils/logger");

const MOCK_RESPONSE = "testResponse";
const MOCK_INPUT_PARAMS = {
  documentId: 1,
  sessionId: 1234
};

describe("KnowledgeArticle.srvc", () => {
  it("autocomplete should return article response", async () => {
    httpClient.sendRequest.mockReturnValueOnce(MOCK_RESPONSE);
    const getArticleResult = await getArticle(MOCK_INPUT_PARAMS);
    expect(getArticleResult).toStrictEqual(MOCK_RESPONSE);
  });
});
