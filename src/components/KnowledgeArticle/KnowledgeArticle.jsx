import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Typography } from "@material-ui/core";
import { parseArticleRaw } from "components/knowledge.parser.js";
import { useArticle } from "./KnowledgeArticle.hooks";
import configService from "components/~common/config.srvc";

import genesysService from "components/~common/genesys.srvc";
import useStyles from "./KnowledgeArticle.styles";
import logger from "~common/utils/logger";

// ------------------------------------------------------------------------------------------------------ component ----

const KnowledgeArticle = ({ data, stopProgress, showProgress, openArticle, config, store }) => {
  const { documentId } = data;
  const { articleState, articleActions } = useArticle({ documentId });
  const { article } = articleState;
  const [articleString, setArticleString] = useState("");
  const { customerId } = store;

  const classes = useStyles();

  // initialize the config
  configService.setConfig({
    ...config,
    appId: config.deploymentId,
    appType: "MessengerKnowledgeApp",
    customerId
  });

  useEffect(() => {
    if (!article) {
      return;
    }

    try {
      const parsedArticleString = article.variations ? parseArticleRaw(article.variations[0].body) : "";
      setArticleString(parsedArticleString);
    } catch (error) {
      const ERROR_TEXT = "We have encountered some problems displaying this article, sorry about that... :(";
      setArticleString(ERROR_TEXT);
      logger.log(ERROR_TEXT, error);
    } finally {
      stopProgress();
      openArticle({
        data,
        route: "knowledge.article",
        showProgress: false
      });
    }
  }, [article]);

  useEffect(() => {
    showProgress(); // call this to support opening direct article view component
    articleActions.getArticleById({ documentId });
  }, [documentId]);

  useEffect(() => {
    (async () => {
      await genesysService.initPlugin();
    })();
  }, []);

  return !article ? null : (
    <div className={classes.container}>
      <h2>{article.title}</h2>
      <Typography variant="body2" dangerouslySetInnerHTML={{ __html: articleString }} />
    </div>
  );
};

// ----------------------------------------------------------------------------------------------------- prop types ----

KnowledgeArticle.propTypes = {
  data: PropTypes.object.isRequired,
  stopProgress: PropTypes.func.isRequired,
  config: PropTypes.object.isRequired,
  showProgress: PropTypes.func.isRequired,
  store: PropTypes.object.isRequired,
  i18n: PropTypes.object.isRequired
};

// -------------------------------------------------------------------------------------------------------- exports ----

export default KnowledgeArticle;
