import React from "react";
import { render } from "@testing-library/react";
import KnowledgeArticle from "components/KnowledgeArticle/KnowledgeArticle.jsx";
import { useArticle } from "./KnowledgeArticle.hooks";
import mockArticle from "components/KnowledgeArticle/mock.article";

jest.mock("components/~common/genesys.srvc");
jest.mock("./KnowledgeArticle.hooks");
jest.mock("components/~common/config.srvc");

const mockOpenArticle = jest.fn();
const mockStopProgress = jest.fn();
const mockShowProgress = jest.fn();
const mockGetArticleById = jest.fn();

const mockProps = {
  data: {
    documentId: 1
  },
  i18n: {},
  openArticle: mockOpenArticle,
  stopProgress: mockStopProgress,
  showProgress: mockShowProgress,
  store: {
    customerId: 1
  },
  config: {
    deploymentId: 1
  }
};

const mockBaseHookResponse = {
  articleState: {
    article: mockOpenArticle
  },
  articleActions: {
    getArticleById: mockGetArticleById
  }
};

useArticle.mockReturnValue(mockBaseHookResponse);

const renderKnowledgeArticle = (props = mockProps) => render(<KnowledgeArticle {...props} />);

describe("KnowledgeArticle", () => {
  beforeEach(() => {
    mockStopProgress.mockClear();
    mockGetArticleById.mockClear();
    mockShowProgress.mockClear();
  });

  it("should render component", () => {
    const testData = mockArticle;
    renderKnowledgeArticle({
      ...mockProps,
      data: testData
    });
  });

  it("should render even when articles doesnt have any articles", () => {
    const testData = {};
    renderKnowledgeArticle({
      ...mockProps,
      data: testData
    });
  });

  it("should call stopProgress when component loaded", () => {
    renderKnowledgeArticle();
    expect(mockStopProgress).toHaveBeenCalledTimes(1);
  });

  it("should call openArticle with appropriate params when component loaded", () => {
    const expectedParams = {
      data: {},
      route: "knowledge.article",
      showProgress: false
    };
    renderKnowledgeArticle();
    expect(mockOpenArticle).toHaveBeenCalledWith(expectedParams);
  });
  it("should call getArticleById when component loaded", () => {
    renderKnowledgeArticle();
    expect(mockBaseHookResponse.articleActions.getArticleById).toHaveBeenCalledTimes(1);
  });
  it("should call showProgress when component loaded", () => {
    renderKnowledgeArticle();
    expect(mockShowProgress).toHaveBeenCalledTimes(1);
  });
});
