import { renderHook, act } from "@testing-library/react-hooks";
import { getConfig } from "components/~common/config.srvc";
import { getSession } from "components/~common/session.srvc";
import { getArticle } from "./KnowledgeArticle.srvc";

import { useArticle } from "./KnowledgeArticle.hooks";

jest.mock("components/~common/session.srvc");
jest.mock("components/~common/config.srvc");
jest.mock("./KnowledgeArticle.srvc");

const MOCK_ARTICLE = "Test article";
const MOCK_DOCUMENT_ID = 1;

getSession.mockReturnValue({ id: 1 });
getConfig.mockReturnValue({ appId: 1, appType: "Test", customerId: 1 });

describe("KnowledgeArticle.hooks", () => {
  describe("useArticle", () => {
    it("should set article to state", async () => {
      getArticle.mockReturnValueOnce(MOCK_ARTICLE);
      const { result } = renderHook(() => useArticle());

      await act(async () => {
        await result.current.articleActions.getArticleById({ documentId: MOCK_DOCUMENT_ID });
      });

      expect(result.current.articleState.article).toEqual(MOCK_ARTICLE);
    });
    it("should set error state on error", async () => {
      getArticle.mockRejectedValueOnce("ERROR");
      const { result } = renderHook(() => useArticle());

      await act(async () => {
        await result.current.articleActions.getArticleById({ documentId: MOCK_DOCUMENT_ID });
      });

      expect(result.current.articleState.articleError).not.toBe(false);
    });
  });
});
