import { getArticle } from "./KnowledgeArticle.srvc";
import { useApiWithSession } from "components/~common/hooks/apiWithSession.hooks";

const useArticle = () => {
  const { apiState, apiActions } = useApiWithSession(getArticle);

  const getArticleById = async ({ documentId }) => {
    await apiActions.execute({ documentId });
  };

  return {
    articleState: {
      articlePending: apiState.pending,
      articleError: apiState.error,
      article: apiState.data
    },
    articleActions: {
      getArticleById
    }
  };
};

export { useArticle };
