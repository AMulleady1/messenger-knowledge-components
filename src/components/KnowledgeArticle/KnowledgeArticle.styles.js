import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  container: {
    padding: "0px 16px",
    overflow: "auto",
    "& img": {
      width: "100%"
    },
    "& iframe": {
      width: "100%",
      border: "none"
    }
  }
});

export default useStyles;
