export default {
  id: "",
  title: "This is a sample title",
  alternatives: [
    {
      phrase: "This is a sample alternative phrase"
    }
  ],
  dateCreated: "2019-10-30T12:48:42.340Z",
  dateModified: "2020-10-20T12:48:42.340Z",
  variations: [
    {
      id: "variationid",
      dateCreated: "2019-10-30T12:48:42.340Z",
      dateModified: "2019-10-30T12:48:42.340Z",
      body: {
        blocks: [
          {
            type: "Paragraph",
            paragraph: {
              blocks: [
                {
                  type: "Text",
                  text: {
                    text: "This text contains "
                  }
                },
                {
                  type: "Text",
                  text: {
                    text: "text in bold",
                    bold: true
                  }
                },
                {
                  type: "Text",
                  text: {
                    text: " and "
                  }
                },
                {
                  type: "Text",
                  text: {
                    text: "italics",
                    italic: true
                  }
                },
                {
                  type: "Text",
                  text: {
                    text: " as well as a "
                  }
                },
                {
                  type: "Text",
                  text: {
                    text: "hyperlink",
                    link: "https://en.wikipedia.org/wiki/Hypertext"
                  }
                },
                {
                  type: "Text",
                  text: {
                    text: "."
                  }
                }
              ]
            }
          },
          {
            type: "Image",
            image: {
              url: "https://excitedcats.com/wp-content/uploads/2021/01/cat-cute.png"
            }
          },
          {
            type: "OrderedList",
            list: {
              blocks: [
                {
                  type: "ListItem",
                  blocks: [
                    {
                      type: "Text",
                      text: {
                        text: " One"
                      }
                    },
                    {
                      type: "Text",
                      text: {
                        text: " Two",
                        bold: true
                      }
                    }
                  ]
                },
                {
                  type: "ListItem",
                  blocks: [
                    {
                      type: "Text",
                      text: {
                        text: " Three"
                      }
                    },
                    {
                      type: "Text",
                      text: {
                        text: " Four",
                        bold: true
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            type: "UnorderedList",
            list: {
              blocks: [
                {
                  type: "ListItem",
                  blocks: [
                    {
                      type: "Text",
                      text: {
                        text: "One"
                      }
                    },
                    {
                      type: "Text",
                      text: {
                        text: " Two",
                        bold: true
                      }
                    }
                  ]
                },
                {
                  type: "ListItem",
                  blocks: [
                    {
                      type: "Text",
                      text: {
                        text: "Three"
                      }
                    },
                    {
                      type: "Text",
                      text: {
                        text: " Four",
                        underline: true
                      }
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      choiceList: {
        items: [
          {
            title: "Option 1",
            content: {
              type: "Text",
              text: "Great!"
            },
            choiceList: {
              items: [
                {
                  title: "Option 1a",
                  content: {
                    type: "Text",
                    text: "Greater!"
                  }
                },
                {
                  title: "Option 1b",
                  content: {
                    type: "Text",
                    text: "Greatest!"
                  }
                }
              ]
            }
          },
          {
            title: "Option 2 (this is a go-to-article)",
            content: {
              type: "Document",
              document: {
                id: "documentid2"
              }
            }
          }
        ]
      },
      contexts: [
        {
          context: {
            id: "contextId"
          },
          values: [
            {
              id: "contextValueId1"
            },
            {
              id: "contextValueId2"
            }
          ]
        }
      ],
      document: {
        id: "documentid"
      }
    }
  ]
};
