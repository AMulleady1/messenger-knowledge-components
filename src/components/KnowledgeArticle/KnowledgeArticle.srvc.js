import httpClient from "components/~common/httpClient.utils";
import config from "~common/config";
import hippodromeConfig from "~common/config/services/hippodrome";
import logger from "~common/utils/logger";

const getArticle = async ({ documentId, sessionId }) => {
  const articleResponse = await httpClient.sendRequest({
    serviceUrl: config.backendUrls.hippodrome,
    serviceOperation: hippodromeConfig.operations.document,
    ids: { sessionId, documentId }
  });

  logger.log("[service] get article return value", articleResponse);
  return articleResponse;
};

export { getArticle };
