import React from "react";
import { screen, render } from "@testing-library/react";
import KnowledgeCard from "components/KnowledgeCard/KnowledgeCard.new";
import userEvent from "@testing-library/user-event";

jest.mock("components/~common/genesys.srvc");
const mockOpenSearch = jest.fn();

describe("KnowledgeCard", () => {
  it("should render component", () => {
    const i18n = {
      knowledge: {
        SearchInputTitle: "",
        SearchInputPlaceholder: ""
      }
    };
    render(<KnowledgeCard i18n={i18n} openSearch={mockOpenSearch} />);
    userEvent.type(screen.getByRole("textbox"), "test");
    expect(mockOpenSearch).toHaveBeenCalled();
  });
});
