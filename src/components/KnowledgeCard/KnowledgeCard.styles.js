import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((_theme) => ({
  container: {
    padding: "16px",
    width: "100%",
    backgroundColor: "transparent!important"
  },
  searchBar: {
    marginTop: "16px",
    width: "100%"
  },
  searchInput: {
    width: "100%"
  },
  searchLabel: {
    display: "flex",
    flexDirection: "row"
  },
  searchIconStartAdorment: {
    paddingLeft: "8px"
  }
}));

export default useStyles;
