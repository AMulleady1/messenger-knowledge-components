import React from "react";
import PropTypes from "prop-types";
import { Card, TextField, Typography, Icon } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { withStyles } from "@material-ui/styles";

const styles = (_theme) => ({
  container: {
    padding: "16px"
  },
  searchBar: {
    marginTop: "16px",
    width: "100%"
  },
  searchInput: {
    width: "100%"
  },
  searchLabel: {
    display: "flex",
    flexDirection: "row"
  },
  searchIconStartAdorment: {
    padding: "8px"
  }
});

class Knowledge extends React.Component {
  constructor(props) {
    super(props);

    this.sPluginName = "Knowledge";
    this.oKnowledge = "";
    this.state = {};

    this.onKeyInput = this.onKeyInput.bind(this);
  }

  onKeyInput(e) {
    if (!e.ctrlKey && !e.shiftKey) {
      e.preventDefault();
      this.props.openSearch({ route: "knowledge.search" });
    }
  }

  registerPlugin() {
    Genesys("registerPlugin", this.sPluginName, (oPlugin) => {
      this.oKnowledge = oPlugin; //TODO: store plugin ref in Launcher and use that if already exists
      oPlugin.ready();
    });
  }

  componentDidMount() {
    this.registerPlugin();
  }

  render() {
    const { classes, i18n, muiCardProps } = this.props;

    return (
      <Card classes={{ root: classes.container }} {...muiCardProps}>
        <form>
          <div className={classes.searchLabel}>
            <Typography variant="h3">{i18n.SearchInputTitle}</Typography>
          </div>
          <TextField
            id="searchBar"
            margin="dense"
            placeholder={i18n.SearchInputPlaceholder}
            variant="outlined"
            classes={{ root: classes.searchBar }}
            onKeyPress={this.onKeyInput}
            onClick={this.onKeyInput} // open the search view when user clicks on the search input in the card
            InputProps={{
              classes: { adornedStart: classes.searchIconStartAdorment },
              startAdornment: (
                <React.Fragment>
                  <Icon color={"inherit"} aria-label="search">
                    <SearchIcon />
                  </Icon>
                </React.Fragment>
              )
            }}
          />
        </form>
      </Card>
    );
  }
}

Knowledge.propTypes = {
  openSearch: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
  muiCardProps: PropTypes.object.isRequired
};

export default withStyles(styles)(Knowledge);
