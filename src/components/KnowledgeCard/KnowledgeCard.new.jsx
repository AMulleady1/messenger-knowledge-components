import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Card, TextField, Typography, Icon } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";

import genesysService from "components/~common/genesys.srvc";
import useStyles from "./KnowledgeCard.styles.js";

// -------------------------------------------------------------------------------------------------- KnowledgeCard ----

const KnowledgeCard = ({ openSearch, i18n, muiCardProps }) => {
  const classes = useStyles();

  const onKeyInput = (e) => {
    if (!e.ctrlKey && !e.shiftKey) {
      e.preventDefault();
      openSearch({ route: "knowledge.search" });
    }
  };

  useEffect(() => {
    (async () => {
      await genesysService.initPlugin();
    })();
  }, []);

  return (
    <Card classes={{ root: classes.container }} {...muiCardProps} onClick={onKeyInput}>
      <form>
        <div className={classes.searchLabel}>
          <Typography variant="h3">{i18n.SearchInputTitle}</Typography>
        </div>
        <TextField
          id="searchBar"
          margin="dense"
          placeholder={i18n.SearchInputPlaceholder}
          variant="outlined"
          autoComplete="off"
          classes={{ root: classes.searchBar }}
          onKeyPress={onKeyInput} // open the search view when user clicks on the search input in the card
          InputProps={{
            classes: { adornedStart: classes.searchIconStartAdorment },
            startAdornment: (
              <React.Fragment>
                <Icon color={"inherit"} aria-label="search" fontSize="small">
                  <SearchIcon fontSize="small" />
                </Icon>
              </React.Fragment>
            )
          }}
        />
      </form>
    </Card>
  );
};

// ------------------------------------------------------------------------------------------------------ proptypes ----

KnowledgeCard.propTypes = {
  openSearch: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
  muiCardProps: PropTypes.object.isRequired
};

// -------------------------------------------------------------------------------------------------------- exports ----

export default KnowledgeCard;
