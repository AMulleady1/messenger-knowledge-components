import React from "react";
import { screen, render } from "@testing-library/react";
import KnowledgeResults from "components/KnowledgeResults/KnowledgeResults";
import userEvent from "@testing-library/user-event";

jest.mock("components/~common/genesys.srvc");

const mockOpenArticle = jest.fn();

describe("KnowledgeResults", () => {
  it("should render component", () => {
    const testDada = { searchResult: [{ document: { id: 1 } }, { document: { id: 2 } }] };
    render(<KnowledgeResults openArticle={mockOpenArticle} i18n={{}} data={testDada} />);

    userEvent.click(screen.getAllByRole("button")[0]);
    expect(mockOpenArticle).toHaveBeenCalled();
  });

  it("should render component with no data", () => {
    const testDada = { searchResult: [] };
    render(<KnowledgeResults openArticle={mockOpenArticle} i18n={{}} data={testDada} />);
  });
});
