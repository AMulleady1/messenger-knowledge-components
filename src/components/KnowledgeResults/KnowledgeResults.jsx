import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { List, ListItem, ListItemText, ListItemSecondaryAction, IconButton, Typography } from "@material-ui/core";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

import genesysService from "components/~common/genesys.srvc";
import useStyles from "./KnowledgeResults.styles";

// ------------------------------------------------------------------------------------------------ KnowledgeResult ----

const KnowledgeResult = (props) => {
  const { document, openArticle } = props;
  const classes = useStyles(props);

  const onArticleClick = (e) => {
    e.preventDefault();
    const data = { documentId: document.id };

    openArticle({ route: "knowledge.article", data, showProgress: true });
  };

  useEffect(() => {
    (async () => {
      await genesysService.initPlugin();
    })();
  }, []);

  return (
    <ListItem button classes={{ secondaryAction: classes.listItemSecondaryAction }} onClick={onArticleClick}>
      <ListItemText
        classes={{
          secondary: classes.listItemText
        }}
        primary={document.primaryText}
        secondary={`secondary text placeholder`}
      />
      <ListItemSecondaryAction>
        <IconButton edge="end" color={"inherit"} aria-label="open article" onClick={onArticleClick}>
          <ChevronRightIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

// ----------------------------------------------------------------------------------------------- KnowledgeResults ----

const KnowledgeResults = (props) => {
  const { data, openArticle, i18n, store } = props;
  const { searchResult, searchQuery } = data;
  const { inputText } = store || {}; // inputText - holds the active input value in the search input field
  const classes = useStyles(props);

  const getSummaryString = ({ searchQuery, searchResult }) => {
    if (!searchResult || !i18n.SearchResultSummary) return "";

    return i18n.SearchResultSummary.replace("<%resultsCount%>", searchResult.length).replace(
      "<%searchQuery%>",
      `<b>"${searchQuery}"</b>`
    );
  };

  const searchSummaryString = getSummaryString({ searchQuery, searchResult });
  const NoSearchResults = () => {
    return (
      <div role="alert" className={classes.searchResultsEmptyContainer}>
        <Typography className={classes.searchResultsEmpty} variant="caption">
          {i18n.SearchResultsEmpty}
        </Typography>
      </div>
    );
  };

  /**
   * TODO: Implement rendering No Results found correctly by checking when the pending search is resolved.
   */

  return !searchResult || searchResult.length < 1 ? (
    inputText ? (
      <NoSearchResults />
    ) : null
  ) : (
    <div>
      <Typography
        className={classes.searchResultSummary}
        variant="caption"
        dangerouslySetInnerHTML={{ __html: searchSummaryString }}
      />
      <List dense={true}>
        {searchResult.map((result) => {
          const { document } = result;
          return <KnowledgeResult key={document.id} document={document} openArticle={openArticle}></KnowledgeResult>;
        })}
      </List>
    </div>
  );
};

// ----------------------------------------------------------------------------------------------------- prop types ----

KnowledgeResults.propTypes = {
  data: PropTypes.object.isRequired,
  openArticle: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired
};

// -------------------------------------------------------------------------------------------------------- exports ----

export default KnowledgeResults;
