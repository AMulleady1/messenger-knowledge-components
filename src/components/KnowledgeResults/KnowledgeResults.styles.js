import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((_theme) => ({
  searchResultSummary: {
    padding: "16px 16px 0 16px",
    display: "block"
  },
  searchResultsEmptyContainer: {
    minHeight: "100%",
    display: "flex",
    boxSizing: "border-box",
    padding: "12px",
    flexGrow: "1",
    alignItems: "center",
    justifyContent: "center"
  },
  searchResultsEmpty: {
    display: "flex",
    textAlign: "center",
    flexDirection: "column",
    flex: "1 1 0%"
  },
  listItemText: {
    clear: "both",
    overflow: "hidden",
    whiteSpace: "nowrap"
  },
  listItemSecondaryAction: {
    padding: "4px 74px 4px 16px"
  }
}));

export default useStyles;
