// --------------------------------------------------------------------------------------------------  leaf parsers ----

const parseText = (textElement) => {
  const { text, marks, hyperlink } = textElement;

  const textWithMarks = !marks
    ? text
    : marks.reduce((currentText, currentMark) => {
        return currentMark === "Bold"
          ? `<b>${currentText}</b>`
          : currentMark === "Italic"
          ? `<i>${currentText}</i>`
          : currentMark === "Underline"
          ? `<u>${currentText}</u>`
          : currentText;
      }, text);

  return hyperlink ? `<a href="${hyperlink}" target="_blank" rel="noreferrer">${textWithMarks}</a>` : textWithMarks;
};

const parseImage = (imageElement) => {
  return `<img src="${imageElement.url}" alt="" />`;
};

const parseVideo = (videoElement) => {
  return `<iframe src="${videoElement.url}"></iframe>`;
};

// --------------------------------------------------------------------------------------------------- node parsers ----

const parseParagraph = (paragraphElement) => {
  const children = paragraphElement.blocks.map((block) => parser(block));
  const childrenString = children.join("");
  return `<p>${childrenString}</p>`;
};

const parseList = (listElement, ordered) => {
  const children = listElement.blocks.map((block) => parser(block));
  const childrenString = children.join("");
  return ordered ? `<ol>${childrenString}</ol>` : `<ul>${childrenString}</ul>`;
};

const parseListItem = (listItemElement) => {
  const children = listItemElement.blocks.map((block) => parser(block));
  const childrenString = children.join("");
  return `<li>${childrenString}</li>`;
};

// --------------------------------------------------------------------------------------------------- block parser ----

const parser = (element) => {
  return element.type === "Text"
    ? parseText(element.text)
    : element.type === "Image"
    ? parseImage(element.image)
    : element.type === "Video"
    ? parseVideo(element.video)
    : element.type === "Paragraph"
    ? parseParagraph(element.paragraph)
    : element.type === "OrderedList"
    ? parseList(element.list, true)
    : element.type === "UnorderedList"
    ? parseList(element.list, false)
    : element.type === "ListItem"
    ? parseListItem(element)
    : null;
};

// ---------------------------------------------------------------------------------------------------- body parser ----

const parseArticleRaw = (articleBody) => {
  const parsedBlocks = articleBody.blocks.map((block) => parser(block));
  return parsedBlocks.join("");
};

// -------------------------------------------------------------------------------------------------------- exports ----

export { parseArticleRaw };
