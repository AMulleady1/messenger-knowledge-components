import { useState } from "react";
import sessionService from "components/~common/session.srvc";
import searchService from "components/KnowledgeSearch/KnowledgeSearch.srvc";
import logger from "~common/utils/logger";

// ---------------------------------------------------------------------------------------------------- use article ----

const useArticle = () => {
  const init = {
    getArticlePending: false,
    getArticleError: false,
    document: null
  };

  const [state, setState] = useState(init);

  const getArticle = async ({ articleId }) => {
    logger.log(`todo: get article by id ${articleId}`);
  };

  const setArticle = ({ document }) => {
    setState({ ...init, document });
  };

  const resetArticle = () => {
    setState(init);
  };

  return {
    articleState: state,
    articleActions: {
      getArticle,
      setArticle,
      resetArticle
    }
  };
};

// ----------------------------------------------------------------------------------------------------- use search ----

const useSearch = ({ appId, appType, customerId }) => {
  const initState = {
    searchQuery: "",
    inputText: "",
    searchPending: false,
    searchError: false,
    searchResult: null
  };

  const [state, setState] = useState(initState);

  const reset = () => {
    // setState((prevState) => ({...initState, searchInputText: prevState.searchInputText}));
    setState({ ...initState });
  };

  const setInputText = ({ text }) => {
    setState((current) => ({ ...current, inputText: text }));
  };

  const search = async ({ searchQuery }) => {
    setState((prevState) => ({
      ...prevState,
      searchQuery,
      searchPending: true,
      searchError: false,
      searchResult: null
    }));

    // TODO: handle expired session errors
    const { id: sessionId } = await sessionService.getSession({ appId, appType, customerId });
    const result = await searchService.search({ sessionId, searchQuery });
    logger.log("[hooks] search result", result);

    setState((prevState) => ({
      ...prevState,
      searchQuery,
      searchPending: false,
      searchError: false,
      searchResult: result
    }));
  };

  return {
    searchState: state,
    searchActions: {
      search,
      setInputText,
      reset
    }
  };
};

// -------------------------------------------------------------------------------------------------- use knowledge ----

const useKnowledge = ({ appId, appType, customerId }) => {
  const { searchState, searchActions } = useSearch({ appId, appType, customerId });
  const { articleState, articleActions } = useArticle();

  const search = async ({ searchQuery }) => {
    try {
      await searchActions.search({ searchQuery });
    } catch (error) {
      logger.log("Error in search", error);
    }
  };

  const wrappedSearchActions = {
    ...searchActions,
    search
  };

  return {
    searchState,
    articleState,
    searchActions: wrappedSearchActions,
    articleActions
  };
};

// -------------------------------------------------------------------------------------------------------- exports ----

export { useKnowledge, useSearch };
