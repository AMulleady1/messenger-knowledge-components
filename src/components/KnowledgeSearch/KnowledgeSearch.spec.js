import React from "react";
import KnowledgeSearch from "./KnowledgeSearch";
import autoCompleteResultSet from "./__mocks__/AutoCompleteResults.mock";
import genesysService from "components/~common/genesys.srvc";
import { debounce } from "./KnowledgeSearch.utils";
import { useSearch } from "./KnowledgeSearch.hooks.js";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

jest.mock("./SearchField/SearchField");
jest.mock("./AutocompleteItem/AutocompleteItem");
jest.mock("./KnowledgeSearch.styles.js");
jest.mock("components/~common/config.srvc");
jest.mock("components/~common/genesys.srvc");
jest.mock("./KnowledgeSearch.hooks.js");
jest.mock("@material-ui/lab/Autocomplete");
jest.mock("./KnowledgeSearch.utils");

const mockObject = {
  reset: jest.fn(),
  clearSearch: jest.fn(),
  setInputText: jest.fn(),
  autocomplete: jest.fn(),
  clearAutocomplete: jest.fn(),
  search: jest.fn(),
  clearInputText: jest.fn()
};

let mockState = {
  searchState: {
    inputText: "",
    searchQuery: "",
    autocompleteQuery: "",
    autocompleteResult: autoCompleteResultSet,
    searchResult: []
  },
  searchActions: {
    reset: mockObject.reset,
    clearSearch: mockObject.clearSearch,
    setInputText: mockObject.setInputText,
    autocomplete: mockObject.autocomplete,
    clearAutocomplete: mockObject.clearAutocomplete,
    search: mockObject.search,
    clearInputText: mockObject.clearInputText
  }
};

const SEARCH_INPUT_PLACEHOLDER = "Mock text";
const STORE_DEFAULT_INPUT_VALuE = "Default text";
const STORE_DEFAULT_CUSTOMER_ID = "1234";
const TEST_TEXT = "Test";

const baseProps = {
  showResults: jest.fn(),
  i18n: { SearchInputPlaceholder: SEARCH_INPUT_PLACEHOLDER },
  config: {
    appId: "2446acf1-d115-40db-9d4e-bf3cd55b5318",
    appType: "SupportCenter"
  },
  store: {
    inputValue: STORE_DEFAULT_INPUT_VALuE,
    customerId: STORE_DEFAULT_CUSTOMER_ID
  },
  setStore: jest.fn(),
  showProgress: jest.fn(),
  stopProgress: jest.fn()
};

const renderKnowledgeSearch = (props = baseProps) => render(<KnowledgeSearch {...props} />);

describe("KnowledgeSearch", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("should render", () => {
    useSearch.mockReturnValue(mockState);
    renderKnowledgeSearch();

    expect(screen.queryByTestId("auto-complete")).not.toBeNull();
  });
  it("should render autocomplete items", () => {
    useSearch.mockReturnValue(mockState);
    renderKnowledgeSearch();

    expect(screen.queryAllByTestId("knowledge-result").length).not.toEqual(0);
  });
  it("should set initial input value from prop", () => {
    useSearch.mockReturnValue(mockState);
    renderKnowledgeSearch();

    expect(mockState.searchActions.setInputText).toHaveBeenCalled();
  });
  it("should init genesys plugin", () => {
    useSearch.mockReturnValue(mockState);
    genesysService.initPlugin = jest.fn();
    renderKnowledgeSearch();

    expect(genesysService.initPlugin).toHaveBeenCalled();
  });
  it("should do search on autocomplete item click", () => {
    useSearch.mockReturnValue(mockState);
    renderKnowledgeSearch();

    const firstResult = screen.getAllByTestId("knowledge-result")[0];
    userEvent.click(firstResult);

    expect(mockState.searchActions.search).toHaveBeenCalled();
  });
  it("should do search on search input enter", () => {
    useSearch.mockReturnValue({ ...mockState, searchState: { ...mockState.searchState, inputText: TEST_TEXT } });
    renderKnowledgeSearch();

    const searchInput = screen.queryByTestId("mock-search");
    userEvent.type(searchInput, `${TEST_TEXT}{enter}`);

    expect(mockState.searchActions.search).toHaveBeenCalled();
  });
  it("should call autocomplete on input type", () => {
    useSearch.mockReturnValue(mockState);
    debounce.mockImplementation((autocompleteQuery) => mockState.searchActions.autocomplete({ autocompleteQuery }));
    renderKnowledgeSearch();

    const searchInput = screen.queryByTestId("mock-search");
    userEvent.type(searchInput, TEST_TEXT);

    expect(mockState.searchActions.autocomplete).toHaveBeenCalled();
  });
  it("should call showProgress on search", () => {
    useSearch.mockReturnValue({ ...mockState, searchState: { ...mockState.searchState, searchPending: true } });
    renderKnowledgeSearch();

    expect(baseProps.showProgress).toHaveBeenCalled();
  });
  it("should not call showProgress if no search happened", () => {
    useSearch.mockReturnValue({ ...mockState, searchState: { ...mockState.searchState, searchPending: false } });
    renderKnowledgeSearch();

    expect(baseProps.showProgress).not.toHaveBeenCalled();
  });
  it("should not call showProgress if no autocomplete happened", () => {
    useSearch.mockReturnValue({ ...mockState, searchState: { ...mockState.searchState, autocompletePending: false } });
    renderKnowledgeSearch();

    expect(baseProps.showProgress).not.toHaveBeenCalled();
  });
  it("should not call showProgress if no search happened", () => {
    useSearch.mockReturnValue({ ...mockState, searchState: { ...mockState.searchState, searchPending: false } });
    renderKnowledgeSearch();

    expect(baseProps.showProgress).not.toHaveBeenCalled();
  });
  it("should call stopProgress if search ended", () => {
    useSearch.mockReturnValue({ ...mockState, searchState: { ...mockState.searchState, searchPending: false } });
    renderKnowledgeSearch();

    expect(baseProps.stopProgress).toHaveBeenCalled();
  });
  it("should call stopProgress if autocomplete ended", () => {
    useSearch.mockReturnValue({ ...mockState, searchState: { ...mockState.searchState, autocompletePending: false } });
    renderKnowledgeSearch();

    expect(baseProps.stopProgress).toHaveBeenCalled();
  });
  it("should call showResults if there is searchResult", () => {
    useSearch.mockReturnValue({
      ...mockState,
      searchState: { ...mockState.searchState, searchResult: [{ document: { title: TEST_TEXT } }] }
    });
    renderKnowledgeSearch();

    expect(baseProps.showResults).toHaveBeenCalled();
  });
  it("should clear after showResult call", () => {
    useSearch.mockReturnValue({
      ...mockState,
      searchState: { ...mockState.searchState, searchResult: [{ document: { title: TEST_TEXT } }] }
    });
    renderKnowledgeSearch();

    expect(mockState.searchActions.clearInputText).toHaveBeenCalled();
    expect(mockState.searchActions.clearAutocomplete).toHaveBeenCalled();
  });
});
