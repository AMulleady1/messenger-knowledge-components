export const autocomplete = jest.fn();
export const search = jest.fn();

export default {
  autocomplete,
  search
};
