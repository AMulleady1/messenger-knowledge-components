const useStyles = jest.fn().mockReturnValue({
  inputRoot: "test",
  endAdornment: "test",
  clearIndicator: "test",
  clearIndicatorDirty: "test",
  option: "test"
});

export default useStyles;
