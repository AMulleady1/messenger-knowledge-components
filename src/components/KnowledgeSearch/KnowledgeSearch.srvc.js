import httpClient from "components/~common/httpClient.utils";
import config from "~common/config";
import hippodromeConfig from "~common/config/services/hippodrome";
import logger from "~common/utils/logger";

// --------------------------------------------------------------------------------------------------- autocomplete ----

const autocomplete = async ({ sessionId, autocompleteQuery }) => {
  const requestData = {
    query: autocompleteQuery,
    pageSize: 3
  };

  // noinspection JSUnresolvedVariable
  const response = await httpClient.sendRequest({
    serviceUrl: config.backendUrls.hippodrome,
    serviceOperation: hippodromeConfig.operations.autoComplete,
    requestData,
    ids: {
      sessionId
    }
  });

  const autocompleteResults = response.results ? response.results : [];
  logger.log("[service] autocomplete parsed results", autocompleteResults);

  return autocompleteResults;
};

// --------------------------------------------------------------------------------------------------------- search ----

const search = async ({ sessionId, searchQuery, pageSize = 3 }) => {
  const requestData = {
    query: searchQuery,
    pageSize,
    pageNumber: 1
  };

  // noinspection JSUnresolvedVariable
  const response = await httpClient.sendRequest({
    serviceUrl: config.backendUrls.hippodrome,
    serviceOperation: hippodromeConfig.operations.search,
    requestData,
    ids: {
      sessionId
    }
  });

  // TODO: this probably breaks the error handling in the hooks layer
  // move response parsing to hooks?
  // or just pass the response error along if there is any
  const searchResults = response.results ? response.results : [];
  logger.log("[service] search parsed results", searchResults);

  return searchResults;
};

// -------------------------------------------------------------------------------------------------------- exports ----

export default {
  autocomplete,
  search
};
