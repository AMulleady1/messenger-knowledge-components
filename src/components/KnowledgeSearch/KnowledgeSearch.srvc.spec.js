import httpClient from "components/~common/httpClient.utils";
import knowledgeSearchService from "components/KnowledgeSearch/KnowledgeSearch.srvc";

jest.mock("components/~common/httpClient.utils");
jest.mock("~common/utils/logger");

const TEST_QUERY = "testQuery";
const TEST_RESULT = [1, 2, 3];

describe("KnowledgeSearch Service", () => {
  it("autocomplete should return with empty array on no response.results from sendRequest", async () => {
    httpClient.sendRequest.mockReturnValueOnce([]);
    const testInput = { sessionId: 1, autoCompleteQuery: TEST_QUERY };
    const autoCompleteResults = await knowledgeSearchService.autocomplete(testInput);
    expect(autoCompleteResults).toStrictEqual([]);
  });

  it("autocomplete should return with the results returned by sendRequest", async () => {
    httpClient.sendRequest.mockReturnValueOnce({ results: TEST_RESULT });
    const testInput = { sessionId: 1, autoCompleteQuery: TEST_QUERY };
    const autoCompleteResults = await knowledgeSearchService.autocomplete(testInput);
    expect(autoCompleteResults).toStrictEqual(TEST_RESULT);
  });

  it("search should return with empty array on no response.results from sendRequest", async () => {
    httpClient.sendRequest.mockReturnValueOnce([]);
    const testInput = { sessionId: 1, searchQuery: TEST_QUERY };
    const autoCompleteResults = await knowledgeSearchService.search(testInput);
    expect(autoCompleteResults).toStrictEqual([]);
  });

  it("search should return with the results returned by sendRequest", async () => {
    httpClient.sendRequest.mockReturnValueOnce({ results: TEST_RESULT });
    const testInput = { sessionId: 1, searchQuery: TEST_QUERY };
    const autoCompleteResults = await knowledgeSearchService.search(testInput);
    expect(autoCompleteResults).toStrictEqual(TEST_RESULT);
  });
});
