import { renderHook, act } from "@testing-library/react-hooks";
import { getConfig } from "components/~common/config.srvc";
import { getSession } from "components/~common/session.srvc";
import { autocomplete, search } from "./KnowledgeSearch.srvc";

import { useSearch } from "./KnowledgeSearch.hooks";

jest.mock("components/~common/session.srvc");
jest.mock("components/~common/config.srvc");
jest.mock("./KnowledgeSearch.srvc");
jest.mock("~common/utils/logger");

const TEST_TEXT = "Test";

getSession.mockReturnValue({ id: 1 });
getConfig.mockReturnValue({ appId: 1, appType: TEST_TEXT, customerId: 1 });
autocomplete.mockReturnValueOnce([TEST_TEXT]).mockRejectedValueOnce("Error").mockReturnValue([TEST_TEXT]);
search.mockReturnValueOnce([TEST_TEXT]).mockRejectedValueOnce("Error").mockReturnValue([TEST_TEXT]);

describe("KnowledgeSearch.hooks", () => {
  describe("autocomplete", () => {
    it("should set result to state", async () => {
      const { result } = renderHook(() => useSearch());

      await act(async () => {
        await result.current.searchActions.autocomplete({ autocompleteQuery: TEST_TEXT });
      });

      expect(result.current.searchState.autocompleteResult.length).not.toBe(0);
    });
    it("should set error state on error", async () => {
      const { result } = renderHook(() => useSearch());

      await act(async () => {
        await result.current.searchActions.autocomplete({ autocompleteQuery: TEST_TEXT });
      });

      expect(result.current.searchState.autocompleteError).not.toBe(false);
    });
  });
  describe("search", () => {
    it("should set result to state", async () => {
      const { result } = renderHook(() => useSearch());

      await act(async () => {
        await result.current.searchActions.search({ searchQuery: TEST_TEXT });
      });

      expect(result.current.searchState.searchResult.length).not.toBe(0);
    });
    it("should set error state on error", async () => {
      const { result } = renderHook(() => useSearch());

      await act(async () => {
        await result.current.searchActions.search({ searchQuery: TEST_TEXT });
      });

      expect(result.current.searchState.searchError).not.toBe(false);
    });
  });
  describe("reset", () => {
    it("should reset the state to default", () => {
      const { result } = renderHook(() => useSearch());

      act(() => {
        result.current.searchActions.setInputText({ text: TEST_TEXT });
      });
      expect(result.current.searchState.inputText).toBe(TEST_TEXT);

      act(() => {
        result.current.searchActions.reset();
      });
      expect(result.current.searchState.inputText).toBe("");
    });
  });
  describe("clearSearch", () => {
    it("should clear the search result", async () => {
      const { result } = renderHook(() => useSearch());

      await act(async () => {
        await result.current.searchActions.search({ searchQuery: TEST_TEXT });
      });
      expect(result.current.searchState.searchResult.length).toBe(1);

      act(() => {
        result.current.searchActions.clearSearch();
      });
      expect(result.current.searchState.searchResult.length).toBe(0);
    });
  });
  describe("clearAutocomplete", () => {
    it("should clear the autoComplete result", async () => {
      const { result } = renderHook(() => useSearch());

      await act(async () => {
        await result.current.searchActions.autocomplete({ autocompleteQuery: TEST_TEXT });
      });
      expect(result.current.searchState.autocompleteResult.length).toBe(1);

      act(() => {
        result.current.searchActions.clearAutocomplete();
      });
      expect(result.current.searchState.autocompleteResult.length).toBe(0);
    });
  });
  describe("setInputText", () => {
    it("should set the input text value", () => {
      const { result } = renderHook(() => useSearch());

      act(() => {
        result.current.searchActions.setInputText({ text: TEST_TEXT });
      });

      expect(result.current.searchState.inputText).toBe(TEST_TEXT);
    });
  });
  describe("clearInputText", () => {
    it("should clear the input text value", () => {
      const { result } = renderHook(() => useSearch());

      act(() => {
        result.current.searchActions.setInputText({ text: TEST_TEXT });
      });
      expect(result.current.searchState.inputText).toBe(TEST_TEXT);

      act(() => {
        result.current.searchActions.clearInputText();
      });
      expect(result.current.searchState.inputText).toBe("");
    });
  });
});
