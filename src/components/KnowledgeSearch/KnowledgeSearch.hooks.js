import { useState } from "react";
import searchService from "components/KnowledgeSearch/KnowledgeSearch.srvc";
import { useApiWithSession } from "components/~common/hooks/apiWithSession.hooks";
import logger from "~common/utils/logger";

// ------------------------------------------------------------------------------------------------------- init state --

const initSearchState = {
  searchQuery: ""
};

const initAutocompleteState = {
  autocompleteQuery: ""
};

const SEARCH_QUERY_MIN_LENGTH = 3;

// ----------------------------------------------------------------------------------------------------- search hooks --

const useSearch = () => {
  const [inputState, setInputState] = useState("");
  const [searchState, setSearchState] = useState(initSearchState);
  const [autocompleteState, setAutocompleteState] = useState(initAutocompleteState);
  const { apiState: autocompleteApiState, apiActions: autocompleteApiActions } = useApiWithSession(
    searchService.autocomplete
  );
  const { apiState: searchApiState, apiActions: searchApiActions } = useApiWithSession(searchService.search);

  const reset = () => {
    setInputState("");
    setSearchState(initSearchState);
    setAutocompleteState(initAutocompleteState);
  };

  const clearSearch = () => {
    setSearchState(initSearchState);
    searchApiActions.clear();
  };

  const clearAutocomplete = () => {
    setAutocompleteState(initAutocompleteState);
    autocompleteApiActions.clear();
  };

  const setInputText = ({ text }) => {
    setInputState(text);
  };

  const clearInputText = () => {
    setInputState("");
  };

  const autocomplete = async ({ autocompleteQuery }) => {
    if (!autocompleteQuery) {
      return;
    }
    setAutocompleteState((prevState) => ({
      ...prevState,
      autocompleteQuery
    }));

    try {
      const result = await autocompleteApiActions.execute({ autocompleteQuery });
      logger.log("[hooks] autocomplete result", result);
    } catch (error) {
      logger.log("Error in autocomplete", error);
    }
  };

  const search = async ({ searchQuery, pageSize = 3 }) => {
    if (searchQuery?.length <= SEARCH_QUERY_MIN_LENGTH) {
      return;
    }
    // Should NOT clear or reset the state after search is done. Value must be retained with in the Search Input box until user clears it explicitly.
    // setInputState("");
    // clearAutocomplete();
    setSearchState({
      searchQuery
    });

    try {
      const result = await searchApiActions.execute({ searchQuery, pageSize });
      logger.log("[hooks] search result", result);
    } catch (error) {
      logger.log("Error in search", error);
    }
  };

  return {
    searchState: {
      inputText: inputState,
      autocompletePending: autocompleteApiState.pending,
      autocompleteError: autocompleteApiState.error,
      autocompleteResult: autocompleteApiState.data ?? [],
      searchPending: searchApiState.pending,
      searchError: searchApiState.error,
      searchResult: searchApiState.data ?? [],
      ...searchState,
      ...autocompleteState
    },
    searchActions: {
      autocomplete,
      search,
      setInputText,
      reset,
      clearSearch,
      clearAutocomplete,
      clearInputText
    }
  };
};

// ---------------------------------------------------------------------------------------------------------- exports --

export { useSearch };
