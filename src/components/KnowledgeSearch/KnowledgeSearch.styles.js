import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((_theme) => ({
  searchCard: {
    padding: "0px"
  },
  searchCardItem: {
    display: "block",
    padding: "16px"
  },
  searchBar: {
    marginTop: "16px",
    width: "100%"
  },
  searchInput: {
    width: "100%"
  },
  searchLabel: {
    display: "flex",
    flexDirection: "row"
  },
  searchIcon: {
    padding: "2px"
  },
  autoCompleteInputRoot: {
    padding: "6px!important"
  },
  autoCompleteEndAdorment: {
    right: "0px!important",
    top: "3px!important"
  },
  autoCompleteClearIndicator: {
    marginRight: "0px"
  },
  autoCompleteOption: {
    display: "block"
  }
}));

export default useStyles;
