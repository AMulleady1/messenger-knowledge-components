import React, { useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import Autocomplete from "@material-ui/lab/Autocomplete";
import SearchField from "./SearchField/SearchField";
import AutocompleteItem from "./AutocompleteItem/AutocompleteItem";

import configService from "components/~common/config.srvc";
import genesysService from "components/~common/genesys.srvc";
import { debounce } from "./KnowledgeSearch.utils";
import { useSearch } from "./KnowledgeSearch.hooks.js";
import useStyles from "./KnowledgeSearch.styles.js";

const autoCompleteOptions = {
  freeSolo: true,
  fullWidth: true,
  autoHighlight: true,
  disablePortal: true
};

const KnowledgeSearch = ({
  showResults,
  i18n,
  config,
  muiAutoCompleteProps,
  muiTextFieldProps,
  store,
  setStore,
  showProgress,
  stopProgress
}) => {
  const classes = useStyles();

  const { searchState, searchActions } = useSearch();
  const { searchPending, autocompletePending, autocompleteResult, searchResult } = searchState;
  const { inputValue, customerId } = store;

  const classDefinitions = {
    inputRoot: classes.autoCompleteInputRoot,
    endAdornment: classes.autoCompleteEndAdorment,
    clearIndicator: classes.autoCompleteClearIndicator,
    option: classes.autoCompleteOption
  };

  const debouncedAutoComplete = useCallback(
    debounce((autocompleteQuery) => {
      searchActions.autocomplete({ autocompleteQuery });
    }, 250),
    []
  );

  const doAutocompleteQuery = (e) => {
    const autocompleteQuery = e.target.value;
    searchActions.setInputText({ text: autocompleteQuery });
    debouncedAutoComplete(autocompleteQuery);
  };

  const handleEnterKeyPress = (e) => {
    if (e.which === 13 && !e.ctrlKey && !e.shiftKey) {
      e.preventDefault();
      const searchQuery = e.target.value;
      doSearch({ searchQuery });
    }
  };

  const handleAutoCompleteItemClick = (matchedPhrase) => {
    doSearch({ searchQuery: matchedPhrase, pageSize: 1 });
  };

  const doSearch = ({ searchQuery, pageSize }) => {
    searchActions.search({ searchQuery, pageSize });
  };

  const handleClear = (e, val, reason) => {
    if (reason === "clear") {
      searchActions.clearInputText();
      searchActions.clearAutocomplete();
    }
  };

  useEffect(() => {
    if (!searchResult?.length) {
      return;
    }
    const results = searchResult.map(({ document }) => ({ document: { ...document, primaryText: document.title } }));

    showResults({ searchResult: results, searchQuery: searchState.searchQuery }, searchState.searchQuery);
    searchActions.clearInputText();
    searchActions.clearAutocomplete();
  }, [searchResult]);

  useEffect(() => {
    if (searchPending) {
      showProgress();
    } else {
      stopProgress();
    }
  }, [searchPending]);

  useEffect(() => {
    if (searchState?.inputText !== store?.inputValue) {
      setStore({ ...store, inputValue: searchState?.inputText });
    }
  }, [searchState]);

  useEffect(() => {
    searchActions.setInputText({ text: inputValue });

    configService.setConfig({
      ...config,
      appId: config.deploymentId,
      appType: "MessengerKnowledgeApp",
      customerId
    });

    (async () => {
      await genesysService.initPlugin();
    })();

    return () => {
      setStore({ ...store, inputText: searchState.inputText });
    };
  }, []);

  const options = autocompleteResult?.map(({ matchedPhrase }) => matchedPhrase);
  const autocompleteVisible = searchState.inputText !== "";

  return (
    <Autocomplete
      id="searchInput"
      value={searchState.inputText}
      classes={classDefinitions}
      loading={autocompletePending}
      options={options}
      onInputChange={handleClear}
      renderInput={(params) => {
        return (
          <SearchField
            params={params}
            muiTextFieldProps={muiTextFieldProps}
            i18n={i18n}
            isLoading={autocompletePending}
            onInputChange={doAutocompleteQuery}
            onSearch={handleEnterKeyPress}
          />
        );
      }}
      renderOption={(option, { inputValue }) => (
        <AutocompleteItem
          displayedText={option}
          highlightedText={inputValue}
          onClick={() => handleAutoCompleteItemClick(option)}
        />
      )}
      {...autoCompleteOptions}
      {...muiAutoCompleteProps}
      open={autocompleteVisible}
    />
  );
};

KnowledgeSearch.propTypes = {
  showResults: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
  muiAutoCompleteProps: PropTypes.object,
  muiTextFieldProps: PropTypes.object,
  store: PropTypes.object.isRequired,
  setStore: PropTypes.func.isRequired,
  showProgress: PropTypes.func.isRequired,
  stopProgress: PropTypes.func.isRequired
};

export default KnowledgeSearch;
