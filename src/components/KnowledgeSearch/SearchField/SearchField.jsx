import React from "react";
import TextField from "@material-ui/core/TextField";
import Icon from "@material-ui/core/Icon";
import CircularProgress from "@material-ui/core/CircularProgress";

import SearchIcon from "@material-ui/icons/Search";
import useStyles from "../KnowledgeSearch.styles";

const SearchField = ({ params, muiTextFieldProps, i18n, isLoading, onSearch, onInputChange }) => {
  const textValue = params.inputProps.value;
  const classes = useStyles();

  return (
    <TextField
      {...params}
      margin="dense"
      autoFocus={true}
      placeholder={i18n.SearchInputPlaceholder}
      onChange={onInputChange}
      onKeyDown={onSearch}
      variant="outlined"
      InputProps={{
        ...params.InputProps,
        startAdornment: (
          <React.Fragment>
            <Icon color={"inherit"} fontSize="small" classes={{ root: classes.searchIcon }}>
              <SearchIcon fontSize="small" />
            </Icon>
          </React.Fragment>
        ),
        endAdornment: (
          <React.Fragment>
            {isLoading ? <CircularProgress color="inherit" size={20} /> : null}
            {!isLoading && textValue ? params.InputProps.endAdornment : null}
          </React.Fragment>
        )
      }}
      {...muiTextFieldProps}
    />
  );
};

export default SearchField;
