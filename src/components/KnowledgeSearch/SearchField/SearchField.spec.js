import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import SearchField from "./SearchField";

jest.mock("@material-ui/core/CircularProgress");

const baseProps = {
  params: {
    inputProps: {
      value: "Test"
    },
    InputProps: {
      endAdornment: <div data-testid="end-adornment" />
    }
  },
  muiTextFieldProps: {},
  i18n: {},
  isLoading: false,
  onSearch: jest.fn(),
  onInputChange: jest.fn()
};

const TEST_TEXT = "Test";

const renderSearchField = (props = baseProps) => render(<SearchField {...props} />);

describe("SearchField", () => {
  it("should render", () => {
    renderSearchField();

    expect(screen.queryByRole("textbox")).not.toBeNull();
  });
  it("should show loading spinner if isLoading true", () => {
    renderSearchField({ ...baseProps, isLoading: true });

    expect(screen.queryByTestId("circular-progress")).not.toBeNull();
  });
  it("should end adornment if isLoading false", () => {
    renderSearchField({ ...baseProps, isLoading: false });

    expect(screen.queryByTestId("end-adornment")).not.toBeNull();
  });
  it("should call input change on type", () => {
    renderSearchField();
    const input = screen.queryByRole("textbox");

    userEvent.type(input, TEST_TEXT);

    expect(baseProps.onInputChange).toHaveBeenCalled();
  });
  it("should call search on keydown", () => {
    renderSearchField();
    const input = screen.queryByRole("textbox");

    userEvent.type(input, TEST_TEXT);

    expect(baseProps.onSearch).toHaveBeenCalled();
  });
});
