import React from "react";

const SearchField = ({ value, onSearch, onChange, params }) => (
  <input
    data-testid="mock-search"
    type="text"
    autoComplete="off"
    value={value}
    onKeyPress={onSearch}
    onChange={onChange}
    {...params}
  />
);

export default SearchField;
