import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import AutocompleteItem from "./AutocompleteItem";
import "@testing-library/jest-dom";

const mockProps = {
  displayedText: "",
  highlightedText: "",
  onClick: jest.fn()
};

const renderSearchResults = (props) => render(<AutocompleteItem {...props} />);

describe("AutocompleteItem", () => {
  it("should show non highlighted text", () => {
    const props = { ...mockProps, displayedText: "Normal text" };
    renderSearchResults(props);

    screen.getByText("Normal text");
  });

  it("should call the click handler", () => {
    const props = { ...mockProps, displayedText: "text foo", highlightedText: "foo" };
    renderSearchResults(props);

    userEvent.click(screen.getByText("text"));

    expect(props.onClick).toHaveBeenCalledTimes(1);
  });

  it("should show highlighted text", () => {
    const props = { ...mockProps, displayedText: "text foo text foo text", highlightedText: "foo" };
    renderSearchResults(props);

    expect(screen.getAllByText("text").length).toEqual(3);
    const elements = screen.getAllByText("foo");
    expect(elements.length).toEqual(2);
    expect(elements[0].className).toContain("highlighted");
  });
});
