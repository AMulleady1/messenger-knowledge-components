import { makeStyles } from "@material-ui/styles";

const useAutoCompleteItemStyles = makeStyles({
  highlighted: {
    fontWeight: 700
  },
  container: {
    whiteSpace: "break-spaces"
  }
});

export default useAutoCompleteItemStyles;
