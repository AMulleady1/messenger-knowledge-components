import React from "react";
import useAutoCompleteItemStyles from "./AutocompleteItem.styles";

const AutocompleteItem = ({ displayedText, highlightedText = "", onClick }) => {
  const classes = useAutoCompleteItemStyles();
  const highlighter = (text, highlight) => {
    const textSections = text.split(new RegExp(`(${highlight})`, "gi"));

    return textSections.map((textSection, i) => {
      return textSection.toLowerCase() === highlightedText.toLowerCase() ? (
        <span key={i} className={classes.highlighted}>
          {textSection}
        </span>
      ) : (
        <span key={i}>{textSection}</span>
      );
    });
  };

  const decoratedText = highlightedText !== "" ? highlighter(displayedText, highlightedText) : displayedText;

  return (
    <div onClick={onClick} className={classes.container}>
      {decoratedText}
    </div>
  );
};

export default AutocompleteItem;
