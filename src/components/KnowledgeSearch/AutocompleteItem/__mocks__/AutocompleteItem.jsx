import React from "react";

const AutocompleteItem = ({ displayedText, highlightedText, onClick }) => (
  <div onClick={onClick} data-testid="knowledge-result">
    <p>{displayedText}</p>
    <p>{highlightedText}</p>
  </div>
);

export default AutocompleteItem;
