import { debounce } from "./KnowledgeSearch.utils.js";

const DEFAULT_TIMEOUT_MS = 1000;
const DEBOUNCE_TEST_PARAM = 10;

let cb = null;

describe("Performance utils", () => {
  describe("Debounce", () => {
    it("should call the callback after the timeout", async () => {
      cb = jest.fn();
      debounce(cb, DEFAULT_TIMEOUT_MS);
      setTimeout(() => {
        expect(cb).toHaveBeenCalled();
      }, DEFAULT_TIMEOUT_MS);
    });
    it("should not call the callback before the timeout", async () => {
      cb = jest.fn();
      debounce(cb, DEFAULT_TIMEOUT_MS);
      expect(cb).not.toHaveBeenCalled();
    });
    it("should cancel the prevous call if new call happens", async () => {
      cb = jest.fn();

      const debouncedFn = debounce(cb, DEFAULT_TIMEOUT_MS);
      debouncedFn();
      debouncedFn();

      setTimeout(() => {
        expect(cb.mock.calls.length).toBe(1);
      }, DEFAULT_TIMEOUT_MS);
    });
    it("should pass arguments from debounce", async () => {
      cb = jest.fn((x) => x);

      const debouncedFn = debounce(cb, DEFAULT_TIMEOUT_MS);
      const num = debouncedFn(DEBOUNCE_TEST_PARAM);

      setTimeout(() => {
        expect(num).toBe(DEBOUNCE_TEST_PARAM);
      }, DEFAULT_TIMEOUT_MS);
    });
  });
});
