import React, { useState } from "react";
import faker from "faker";

import KnowledgeSearch from "components/KnowledgeSearch/KnowledgeSearch.jsx";
import KnowledgeArticle from "components/KnowledgeArticle/KnowledgeArticle.jsx";
import KnowledgeResults from "components/KnowledgeResults/KnowledgeResults.jsx";
import logger from "~common/utils/logger";

import useStyles from "./KnowledgeWrapper.styles";

// Note: en-us language is hardcoded for local machine development and testing
import i18n from "~common/i18n/en-us";

// ------------------------------------------------------------------------------------------------------ constants ----

const COMPONENTS = {
  search: "knowledge-card",
  article: "knowledge-article",
  results: "knowledge-results"
};

// ---------------------------------------------------------------------------------------------------------- hooks ----

const useWrapper = () => {
  const initState = {
    activeComponent: COMPONENTS.results,
    searchConfig: {
      appType: "SupportCenter",
      deploymentId: "be818bb7-09d8-49c0-9cfc-7a321844e49c"
    },
    articleData: {},
    resultData: {},
    store: {
      inputValue: "",
      customerId: faker.datatype.uuid()
    }
  };

  const [state, setState] = useState(initState);

  const setActiveComponent = (componentName) => {
    setState((prevState) => ({ ...prevState, activeComponent: componentName }));
  };

  const setStore = (store) => {
    setState((prevState) => ({ ...prevState, store }));
  };

  const displayResults = (data) => {
    setState((prevState) => ({ ...prevState, activeComponent: COMPONENTS.results, resultData: data }));
  };

  const displayArticle = ({ data }) => {
    setState((prevState) => ({ ...prevState, activeComponent: COMPONENTS.article, articleData: data }));
  };

  const resetState = () => setState(initState);

  return [
    state,
    {
      setActiveComponent,
      resetState,
      displayResults,
      displayArticle,
      setStore
    }
  ];
};

// ------------------------------------------------------------------------------------------------------ component ----

const KnowledgeWrapper = () => {
  const [state, actions] = useWrapper();
  const classes = useStyles();

  const showResults = (data, inputText) => {
    actions.setStore({ ...state.store, inputValue: inputText });
    logger.log("results view initiated with:", data);

    actions.displayResults(data);
  };

  const showProgress = () => {
    logger.log("show progress called");
  };

  const stopProgress = () => {
    logger.log("stop progress called");
  };

  const openArticle = ({ route, data }) => {
    logger.log(`article view initiated with route ${route} and data ${data}`);
    actions.displayArticle({ route, data });
  };

  return (
    <div className={classes.container}>
      <div className={classes.widgetControls}>
        <button onClick={() => actions.setActiveComponent(COMPONENTS.results)}>results</button>
        <button onClick={() => actions.setActiveComponent(COMPONENTS.article)}>article</button>
        <button onClick={() => actions.resetState()}>reset</button>
      </div>
      <div className={classes.widgetContainer}>
        <div className={classes.searchContainer}>
          {/* <KnowledgeCard i18n={i18n.konwledge} openSearch={openSearch} /> */}
          <KnowledgeSearch
            config={state.searchConfig}
            i18n={i18n.knowledge}
            store={state.store}
            setStore={actions.setStore}
            showProgress={showProgress}
            stopProgress={stopProgress}
            showResults={showResults}
          ></KnowledgeSearch>
        </div>
        <div className={classes.resultsContainer}>
          {state.activeComponent === COMPONENTS.article ? (
            <KnowledgeArticle
              data={state.articleData}
              stopProgress={stopProgress}
              i18n={i18n.knowledge}
              openArticle={openArticle}
              config={state.searchConfig}
              store={state.store}
              showProgress={showProgress}
            ></KnowledgeArticle>
          ) : (
            <KnowledgeResults
              data={state.resultData}
              i18n={i18n.knowledge}
              openArticle={openArticle}
            ></KnowledgeResults>
          )}
        </div>
      </div>
    </div>
  );
};

// -------------------------------------------------------------------------------------------------------- exports ----

export default KnowledgeWrapper;
