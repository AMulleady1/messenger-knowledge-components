import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((_theme) => ({
  container: {
    height: "100%",
    width: "100%",

    display: "flex",
    flexDirection: "column",

    padding: "20px",
    overflow: "auto",

    backgroundColor: "#FCFCFC",
    border: "1px solid lightgray",
    borderRadius: "4px"
  },

  widgetControls: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginBottom: "10px",

    "& button": {
      backgroundColor: "#F5F5F5",
      border: "none",
      borderRadius: "4px",
      padding: "10px 20px",
      marginRight: "20px"
    },

    "& button:hover": {
      backgroundColor: "cornflowerblue",
      color: "white"
    }
  },

  widgetContainer: {
    display: "flex",
    flexDirection: "column",
    flex: "1",
    overflow: "hidden"
  },

  searchContainer: {},

  resultsContainer: {
    flex: "1",
    overflow: "auto"
  }
}));

export default useStyles;
