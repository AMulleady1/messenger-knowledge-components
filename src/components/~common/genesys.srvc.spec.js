import genesysService from "./genesys.srvc";
import { getConfig } from "components/~common/config.srvc";
import { mockPlugin } from "components/~common/__mocks__/Genesys";

jest.mock("components/~common/config.srvc");

const FALLBACK_STORAGE = { get: "LocalStorage.get", set: "LocalStorage.set" };
const TEST_CONFIG_1 = { storage: "LocalStorage" };
const TEST_CONFIG_2 = { storage: undefined };
const TEST_CONFIG_3 = undefined;
const TEST_STORAGE_KEY = "key";
const TEST_STORAGE_VALUE = "test value";

describe("genesys.srvc", () => {
  describe("getStorageCommands", () => {
    it("should return storage plugin from config", () => {
      getConfig.mockReturnValueOnce(TEST_CONFIG_1);

      const result = genesysService.getStorageCommands();

      expect(result).toStrictEqual(FALLBACK_STORAGE);
    });
    it("should fallback if config value is undefined", () => {
      getConfig.mockReturnValueOnce(TEST_CONFIG_2);

      const result = genesysService.getStorageCommands();

      expect(result).toEqual(FALLBACK_STORAGE);
    });
    it("should fallback if config is undefined", () => {
      getConfig.mockReturnValueOnce(TEST_CONFIG_3);

      const result = genesysService.getStorageCommands();

      expect(result).toEqual(FALLBACK_STORAGE);
    });
  });

  describe("getFromStorage", () => {
    it("should return with expected response", async () => {
      mockPlugin.command.mockReturnValueOnce(TEST_STORAGE_VALUE);

      const result = await genesysService.getFromStorage(TEST_STORAGE_KEY);

      expect(result).toEqual(TEST_STORAGE_VALUE);
    });

    it("should return null when key not found", async () => {
      const mockResponse = `${TEST_STORAGE_KEY} not found`;
      mockPlugin.command.mockReturnValueOnce(mockResponse);

      const result = await genesysService.getFromStorage(TEST_STORAGE_KEY);

      expect(result).toEqual(null);
    });

    it("should call 'LocalStorage.get' plugin command with appropriate parameters", async () => {
      const expectedParams = { name: TEST_STORAGE_KEY };

      await genesysService.getFromStorage(TEST_STORAGE_KEY);

      expect(mockPlugin.command).toHaveBeenCalledWith(FALLBACK_STORAGE.get, expectedParams);
    });
  });

  describe("saveToStorage", () => {
    it("should call 'LocalStorage.set' plugin command with appropriate parameters", async () => {
      const params = { name: TEST_STORAGE_KEY, value: TEST_STORAGE_VALUE };

      await genesysService.saveToStorage(params);

      expect(mockPlugin.command).toHaveBeenCalledWith(FALLBACK_STORAGE.set, params);
    });
  });
});
