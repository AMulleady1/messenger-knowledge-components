import genesysConfig from "~common/config/services/genesys";
import configService from "components/~common/config.srvc";

let pluginPromise = null;

const getStorageCommands = () => {
  const storageType = configService.getConfig()?.storage || "LocalStorage";
  return { get: `${storageType}.get`, set: `${storageType}.set` };
};

const sendRequest = async ({ url, method, requestData }) => {
  const plugin = await getPlugin();
  return plugin.command("MessengerHelper.makeRequest", {
    url,
    method,
    data: requestData
  });
};

const getPlugin = async () => {
  if (pluginPromise === null) {
    pluginPromise = registerPlugin();
  }

  return pluginPromise;
};

const getFromStorage = async (name) => {
  const plugin = await getPlugin();
  const pluginReturnValue = await plugin.command(getStorageCommands().get, { name });
  return pluginReturnValue === `${name} not found` ? null : pluginReturnValue;
};

const saveToStorage = async ({ name, value }) => {
  const plugin = await getPlugin();
  await plugin.command(getStorageCommands().set, { name, value });
};

const registerPlugin = () => {
  return new Promise((resolve) => {
    // noinspection JSUnresolvedFunction
    Genesys("registerPlugin", genesysConfig.plugin.name, (oPlugin) => {
      oPlugin.ready();
      resolve(oPlugin);
    });
  });
};

const publish = async (message, data) => {
  const plugin = await getPlugin();
  plugin.publish(message, data);
};

const log = async (message, data) => {
  const plugin = await getPlugin();
  plugin.log(message, data);
};

export default {
  getFromStorage,
  saveToStorage,
  sendRequest,
  getPlugin,
  initPlugin: getPlugin,
  getStorageCommands,
  publish,
  log
};
