import config from "~common/config";
import genesysService from "components/~common/genesys.srvc";
import interceptors from "~common/utils/http/interceptors";
import noRetryDelegates from "~common/utils/http/noRetryDelegates";
import url from "~common/utils/http/url";
import logger from "~common/utils/logger";

const {
  addBeforeRequestInterceptor,
  addAfterRequestInterceptor,
  runBeforeRequestInterceptors,
  runAfterRequestInterceptors
} = interceptors;

const { addNoRetryDelegates, runNoRetryDelegates } = noRetryDelegates;

const { composeUrl } = url;

const sendRequest = async ({ serviceUrl, serviceOperation, requestData, ids }) => {
  const beforeContext = await runBeforeRequestInterceptors({ serviceUrl, serviceOperation, requestData, ids });
  const responseData = await sendRequestCore(beforeContext);
  const afterContext = await runAfterRequestInterceptors({ responseData, ...beforeContext });
  return afterContext.responseData;
};

const sendRequestCore = async ({ serviceUrl, serviceOperation, requestData, ids = {} }) => {
  // noinspection JSUnresolvedVariable
  const absoluteUrl = serviceOperation.usePublicApi ? config.backendUrls.publicApi : serviceUrl;
  const relativeUrl = composeUrl(serviceOperation.url, ids);

  const url = absoluteUrl + relativeUrl;
  const method = serviceOperation.method;

  let retries = 0;
  const maxRetries = 3;

  while (retries < maxRetries) {
    try {
      logger.log(`[service] send request to: ${method.toUpperCase()} ${url}`, requestData);
      const response = await genesysService.sendRequest({ url, method, requestData });
      logger.log("[service] response received", response.data);
      return response.data;
    } catch (error) {
      if (error.data && !runNoRetryDelegates(error.data)) {
        return error.data;
      }
      logger.log(
        `[service] http call failed on url ${method.toUpperCase()} ${url} 
        with status ${error.status}, Try ${retries + 1}`,
        error
      );
      retries++;
    }
  }
  throw new Error(`Http call failed after ${maxRetries} tries on url ${method.toUpperCase()} ${url}`);
};

export default {
  sendRequest,
  addNoRetryDelegates,
  register: {
    beforeRequestInterceptor: addBeforeRequestInterceptor,
    afterRequestInterceptor: addAfterRequestInterceptor
  }
};
