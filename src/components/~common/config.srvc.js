import logger from "~common/utils/logger";

let config = null;

const setConfig = (configuration) => {
  logger.log("[service] set config", configuration);
  config = configuration;
};

const getConfig = () => {
  logger.log("[service] get config", config);
  return config;
};

export default {
  setConfig,
  getConfig
};
