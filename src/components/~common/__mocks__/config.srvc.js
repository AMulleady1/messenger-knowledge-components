export const getConfig = jest.fn();
export const setConfig = jest.fn();

export default {
  getConfig,
  setConfig
};
