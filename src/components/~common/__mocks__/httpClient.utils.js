const sendRequest = jest.fn();
const addNoRetryDelegates = jest.fn();
const beforeRequestInterceptor = jest.fn();
const afterRequestInterceptor = jest.fn();

export default {
  sendRequest,
  addNoRetryDelegates,
  register: {
    beforeRequestInterceptor,
    afterRequestInterceptor
  }
};
