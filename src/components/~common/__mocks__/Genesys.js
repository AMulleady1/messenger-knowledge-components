const mockGenesys = jest.fn((command, pluginName, callback) => {
  callback(mockPlugin);
});

const mockPlugin = {
  subscribe: jest.fn((eventName, callback) => {
    callback();
  }),
  ready: jest.fn(),
  command: jest.fn()
};

window["Genesys"] = mockGenesys;

export { mockGenesys, mockPlugin };
