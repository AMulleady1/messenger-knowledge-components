export const getPlugin = jest.fn();
export const getCustomerId = jest.fn();
export const initPlugin = jest.fn();
export const sendRequest = jest.fn();
export const getFromStorage = jest.fn();
export const saveToStorage = jest.fn();
export const getStorageCommands = jest.fn();
export const publish = jest.fn();
export const log = jest.fn();

export default {
  initPlugin,
  getPlugin,
  getCustomerId,
  sendRequest,
  getFromStorage,
  saveToStorage,
  getStorageCommands,
  publish,
  log
};
