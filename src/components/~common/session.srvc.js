import httpClient from "components/~common/httpClient.utils";
import config from "~common/config";
import configService from "components/~common/config.srvc";
import hippodromeConfig from "~common/config/services/hippodrome";
import genesysService from "components/~common/genesys.srvc";
import logger from "~common/utils/logger";

let session = null;
let createSessionPromise = null;

// -------------------------------------------------------------------------------------------- session persistence ----

const sessionIdStorageKey = (appId) => `${appId}:gcmksid`;

const getSessionFromStorage = async (appId) => {
  logger.log("[service] checking session storage");

  const sessionId = await genesysService.getFromStorage(sessionIdStorageKey(appId));
  if (!sessionId) {
    logger.log("[service] sessionId not found in storage");
    return null;
  }

  logger.log("[service] session found in storage");
  return { id: sessionId };
};

const saveSessionToStorage = async ({ appId, session }) => {
  logger.log("[service] saving session to session storage");
  genesysService.saveToStorage({ name: sessionIdStorageKey(appId), value: session.id });
};

// ------------------------------------------------------------------------------------------------- create session ----

const createSession = async ({ appId, appType, customerId }) => {
  logger.log("[service] - new - create session called");
  session = await getSessionFromStorage(appId);

  return session
    ? session
    : createSessionPromise
    ? createSessionPromise
    : createNewSession({ appId, appType, customerId });
};

const renewSession = async ({ appId, appType, customerId }) => {
  logger.log("[service] - new - renew session called");
  return createSessionPromise ? createSessionPromise : createNewSession({ appId, appType, customerId });
};

const createNewSession = async ({ appId, appType, customerId }) => {
  const requestData = {
    app: {
      deploymentId: appId,
      type: appType
    },
    customerId: customerId,
    pageUrl: window.location.href
  };

  // noinspection JSUnresolvedVariable
  createSessionPromise = httpClient.sendRequest({
    serviceUrl: config.backendUrls.hippodrome,
    serviceOperation: hippodromeConfig.operations.createSession,
    requestData
  });

  session = await createSessionPromise;
  createSessionPromise = null;
  saveSessionToStorage({ appId, session });
  return session;
};

// -------------------------------------------------------------------------------------------- session interceptor ----

const isSessionExpired = (responseData) => responseData.code === "session.not.found";
httpClient.addNoRetryDelegates(isSessionExpired);

httpClient.register.afterRequestInterceptor(async (context) => {
  if (isSessionExpired(context.responseData)) {
    const { appId, appType, customerId } = configService.getConfig();
    const updatedSession = await renewSession({ appId, appType, customerId });
    // Repeat original http call
    context.ids.sessionId = updatedSession.id;
    context.responseData = await httpClient.sendRequest(context);
  }

  return context;
});

// -------------------------------------------------------------------------------------------------------- exports ----

export default {
  createSession,
  getSession: createSession,
  renewSession
};
