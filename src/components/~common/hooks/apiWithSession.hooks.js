import { useState } from "react";
import configService from "components/~common/config.srvc";
import sessionService from "components/~common/session.srvc";

const initState = {
  pending: false,
  error: false,
  data: null
};

const useApiWithSession = (apiCall) => {
  const [state, setState] = useState(initState);

  const execute = async (params) => {
    setState((prevState) => ({
      ...prevState,
      pending: true
    }));

    try {
      const { appId, appType, customerId } = configService.getConfig();
      const { id: sessionId } = await sessionService.getSession({ appId, appType, customerId });
      const response = await apiCall({ sessionId, ...params });
      setState((prevState) => ({
        ...prevState,
        pending: false,
        data: response
      }));
      return response;
    } catch (err) {
      setState((prevState) => ({
        ...prevState,
        pending: false,
        error: err
      }));
    }
  };

  const clear = () => {
    setState(initState);
  };

  return {
    apiState: state,
    apiActions: {
      execute,
      clear
    }
  };
};

export { useApiWithSession };
