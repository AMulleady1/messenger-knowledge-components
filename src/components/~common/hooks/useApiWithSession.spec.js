import { renderHook, act } from "@testing-library/react-hooks";
import { getConfig } from "components/~common/config.srvc";
import { getSession } from "components/~common/session.srvc";

import { useApiWithSession } from "./apiWithSession.hooks";

jest.mock("components/~common/session.srvc");
jest.mock("components/~common/config.srvc");

const mockApiCall = jest.fn();
const mockApiResponse = {
  value: "test"
};
const mockApiParams = {
  value: 1
};

const mockSessionResponse = { id: 1 };

getSession.mockReturnValue({ id: 1 });
getConfig.mockReturnValue({ appId: 1, appType: "Test", customerId: 1 });

describe("apiWithSession.hooks", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  describe("execute", () => {
    it("should invoke apiCall function from hook", async () => {
      const { result } = renderHook(() => useApiWithSession(mockApiCall));

      await act(async () => {
        await result.current.apiActions.execute(mockApiParams);
      });
      expect(mockApiCall).toHaveBeenCalledTimes(1);
    });
    it("should execute api with function param and sessionId", async () => {
      const { result } = renderHook(() => useApiWithSession(mockApiCall));

      await act(async () => {
        await result.current.apiActions.execute(mockApiParams);
      });
      const apiCallInvokeParameters = mockApiCall.mock.calls[0][0];

      expect(apiCallInvokeParameters.value).toEqual(mockApiParams.value);
      expect(apiCallInvokeParameters.sessionId).toEqual(mockSessionResponse.id);
    });
    it("should set response data to state", async () => {
      mockApiCall.mockReturnValueOnce(mockApiResponse);
      const { result } = renderHook(() => useApiWithSession(mockApiCall));

      await act(async () => {
        await result.current.apiActions.execute(mockApiParams);
      });
      expect(result.current.apiState.data).toEqual(mockApiResponse);
    });
    it("should set error data to state n error", async () => {
      const errorMsg = "Error";
      mockApiCall.mockRejectedValueOnce(errorMsg);
      const { result } = renderHook(() => useApiWithSession(mockApiCall));

      await act(async () => {
        await result.current.apiActions.execute(mockApiParams);
      });
      expect(result.current.apiState.error).toEqual(errorMsg);
    });
  });
});
