jest.mock("components/~common/httpClient.utils");
jest.mock("components/~common/config.srvc");
jest.mock("components/~common/genesys.srvc");
jest.mock("~common/utils/logger");

const TEST_PARAMS = {
  appId: "1234",
  appType: "SupportCenter",
  customerId: "e41815ad-5cf8-49c0-9044-39b84710fb02"
};

const TEST_SESSION_1 = { id: 1 };
const TEST_SESSION_2 = { id: 2 };

describe("session.srvc", () => {
  let sessionSrvc;
  let httpClient;
  let genesysSrvc;

  beforeEach(async () => {
    jest.resetModules();
    jest.clearAllMocks();
    const sessionSrvcModule = await import("./session.srvc");
    const httpClientModule = await import("components/~common/httpClient.utils");
    const genesysSrvcModule = await import("components/~common/genesys.srvc");
    sessionSrvc = sessionSrvcModule.default;
    httpClient = httpClientModule.default;
    genesysSrvc = genesysSrvcModule.default;
  });

  it("should register session expired delegate", async () => {
    expect(httpClient.addNoRetryDelegates).toHaveBeenCalledTimes(1);
  });

  it("should register after request interceptor", async () => {
    expect(httpClient.register.afterRequestInterceptor).toHaveBeenCalledTimes(1);
  });

  describe("createSession", () => {
    it("should create new session if no session is created", async () => {
      httpClient.sendRequest.mockReturnValueOnce(TEST_SESSION_1);
      genesysSrvc.getFromStorage.mockReturnValueOnce(null);
      const { createSession } = sessionSrvc;

      const session = await createSession(TEST_PARAMS);

      expect(session).toEqual(TEST_SESSION_1);
    });
    it("should return existing session if it is created", async () => {
      httpClient.sendRequest.mockReturnValueOnce(TEST_SESSION_1).mockReturnValueOnce(TEST_SESSION_2);
      genesysSrvc.getFromStorage.mockReturnValueOnce(null).mockReturnValue(TEST_SESSION_1.id);
      const { createSession } = sessionSrvc;

      await createSession(TEST_PARAMS);
      const secondSession = await createSession(TEST_PARAMS);

      expect(secondSession).toEqual(TEST_SESSION_1);
    });
  });
  describe("renewSession", () => {
    it("should renew the existing session", async () => {
      httpClient.sendRequest.mockReturnValueOnce(TEST_SESSION_1).mockReturnValueOnce(TEST_SESSION_2);
      genesysSrvc.getFromStorage.mockReturnValueOnce(null);
      const { createSession, renewSession } = sessionSrvc;

      await createSession(TEST_PARAMS);
      const renewedSession = await renewSession(TEST_PARAMS);

      expect(renewedSession).toEqual(TEST_SESSION_2);
    });
  });
});
