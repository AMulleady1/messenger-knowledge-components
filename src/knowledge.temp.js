import React from "react";
import PropTypes from "prop-types";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {
  Card,
  TextField,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Icon
} from "@material-ui/core";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import CircularProgress from "@material-ui/core/CircularProgress";
import SearchIcon from "@material-ui/icons/Search";
import { withStyles, makeStyles } from "@material-ui/styles";
import parse from "autosuggest-highlight/parse";
import match from "autosuggest-highlight/match";

const styles = (_theme) => ({
  searchCard: {
    padding: "0px"
  },
  searchCardItem: {
    display: "block",
    padding: "16px"
  },
  searchBar: {
    marginTop: "16px",
    width: "100%"
  },
  searchInput: {
    width: "100%"
  },
  searchLabel: {
    display: "flex",
    flexDirection: "row"
  },
  searchIcon: {
    right: "16px",
    position: "absolute"
  },
  autoCompleteInputRoot: {
    padding: "6px!important"
  },
  autoCompleteEndAdorment: {
    right: "0px!important",
    top: "3px!important"
  },
  autoCompleteClearIndicator: {
    marginRight: "0px"
  }
});

const useStyles = makeStyles((_theme) => ({
  searchResultSummary: {
    padding: "16px 16px 0 16px",
    display: "block"
  },
  listItemText: {
    clear: "both",
    overflow: "hidden",
    whiteSpace: "nowrap"
  },
  listItemSecondaryAction: {
    padding: "4px 74px 4px 16px"
  }
}));

/** TEMPORARY DUMMY SEARCH RESULT DATA - TO BE REMOVED */

const dummyResults = [
  {
    primaryText: "Search Result 1",
    secondaryText: "Loreum epsum 1 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 2",
    secondaryText: "Loreum epsum 2 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 3",
    secondaryText: "Loreum epsum 3 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 4",
    secondaryText: "Loreum epsum 4 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 5",
    secondaryText: "Loreum epsum 5 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 6",
    secondaryText: "Loreum epsum 6 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 7",
    secondaryText: "Loreum epsum 7 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 8",
    secondaryText: "Loreum epsum 8 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 9",
    secondaryText: "Loreum epsum 9 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  },
  {
    primaryText: "Search Result 10",
    secondaryText: "Loreum epsum 10 Loreum epsum Loreum epsum Loreum epsum Loreum epsum Loreum epsum"
  }
];

const dummyAutoSuggestedOptions = [
  "Dishwasher issue",
  "Refrigerator issue",
  "Billing issue",
  "Account access issue",
  "Genesys Telecommunications",
  "other issue",
  "Refrigerator issue",
  "Billing issue",
  "Account access issue",
  "Genesys Telecommunications",
  "other issue",
  "Refrigerator issue",
  "Billing issue",
  "Account access issue",
  "Genesys Telecommunications",
  "other issue",
  "Refrigerator issue",
  "Billing issue",
  "Account access issue",
  "Genesys Telecommunications",
  "other issue"
];

/********************* Article component *************************/

const KnowledgeArticle = () => <div></div>;

/********************* Search Results component *************************/

const KnowledgeResults = (props) => {
  const classes = useStyles(props);
  const { data, i18n, store } = props;
  const { inputValue } = store || {};
  const iResultsCount = data && Object.keys(data).length;

  /*************** localized Results summary string with count and input query ***************/
  let sI18nSearchSummary = i18n.SearchResultSummary.replace("<%resultsCount%>", iResultsCount);
  sI18nSearchSummary = sI18nSearchSummary.replace("<%searchQuery%>", `<b>"${inputValue}"</b>`);
  /*******************************************************************************************/

  const onIconClick = (e) => {
    e.preventDefault();
    props.openArticle({ route: "knowledge.article", showProgress: true });
    setTimeout(() => {
      props.openArticle({ route: "knowledge.article", data: { name: "Ranjith" }, showProgress: false });
    }, 3000);
  };

  return (
    <div>
      {iResultsCount ? ( // show search result summary and count
        <Typography
          className={classes.searchResultSummary}
          variant="caption"
          dangerouslySetInnerHTML={{ __html: sI18nSearchSummary }}
        />
      ) : null}

      <List dense={true}>
        {iResultsCount
          ? data.map((oResult, index) => {
              return (
                <ListItem
                  key={index}
                  button
                  classes={{
                    secondaryAction: classes.listItemSecondaryAction
                  }}
                  onClick={onIconClick}
                >
                  <ListItemText
                    classes={{
                      secondary: classes.listItemText
                    }}
                    primary={oResult.primaryText}
                    secondary={oResult.secondaryText}
                  />
                  <ListItemSecondaryAction>
                    <IconButton edge="end" color={"inherit"} aria-label="open article" onClick={onIconClick}>
                      <ChevronRightIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              );
            })
          : null}
      </List>
    </div>
  );
};

/********************* Search Input box component in the Header *************************/

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null,
      open: false,
      loading: false,
      suggestedOptions: []
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.setOpen = this.setOpen.bind(this);
    this.setClose = this.setClose.bind(this);
  }

  onKeyPress(e) {
    // Add logic to search and make request when user hits enter key WITHOUT WAITING for the search results
    if (e.which == 13 && !e.ctrlKey && !e.shiftKey) {
      // eslint-disable-next-line no-undef
      that.props.showResults(dummyResults); // pass data
      e.preventDefault();
    }
  };

  /*************** Handle input typed by the user  *****************/
  onInputChange(e, val, reason) {
    // Add logic to fetch auto suggested options and set the state
    const that = this;

    if (reason == "input" && val) {
      // show loading symbol
      this.setState({
        loading: true
      });

      setTimeout(() => {
        that.setState({
          loading: false,
          suggestedOptions: dummyAutoSuggestedOptions
        });
      }, 2000);
    } else if (reason == "reset" && val && e && !e.ctrlKey && !e.shiftKey) {
      // Add logic to search and make request when user hits enter key or selects an option from the list
      this.setState({ value: val });
      that.props.showProgress();
      setTimeout(() => {
        that.props.showResults(dummyResults, val); // pass data
      }, 3000);
    }
  }

  /****************** show loading popper ********************/
  setOpen() {
    this.setState({
      open: true
    });
  }

  /****************** close loading popper ********************/
  setClose() {
    this.setState({
      open: false
    });
  }

  render() {
    const { classes, i18n, muiAutoCompleteProps, muiTextFieldProps, store } = this.props;
    const { inputValue } = store || {};
    const { suggestedOptions, loading, value } = this.state;

    return (
      <Autocomplete
        id="searchInput"
        value={value || inputValue}
        classes={{
          inputRoot: classes.autoCompleteInputRoot,
          endAdornment: classes.autoCompleteEndAdorment,
          clearIndicator: classes.autoCompleteClearIndicator
        }}
        freeSolo={true}
        fullWidth={true}
        autoHighlight={true}
        onOpen={this.setOpen}
        onClose={this.setClose}
        disablePortal={true}
        onInputChange={this.onInputChange}
        loading={loading}
        options={suggestedOptions}
        getOptionLabel={(option) => {
          return option;
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            margin="dense"
            autoFocus={true}
            placeholder={i18n.SearchInputPlaceholder}
            variant="outlined"
            //onKeyPress={this.onKeyPress} // TODO
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {!loading ? params.InputProps.endAdornment : null}
                </React.Fragment>
              )
            }}
            {...muiTextFieldProps}
          />
        )}
        renderOption={(option, { inputValue }) => {
          const matches = match(option, inputValue);
          const parts = parse(option, matches);

          return (
            <div>
              {parts.map((part, index) => (
                <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                  {part.text}
                </span>
              ))}
            </div>
          );
        }}
        {...muiAutoCompleteProps}
      />
    );
  }
}

Search.propTypes = {
  showResults: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired
};

/********************* Search Card component *************************/

class Knowledge extends React.Component {
  constructor(props) {
    super(props);

    this.sPluginName = "Knowledge";
    this.oKnowledge = "";
    this.state = {};

    this.onKeyInput = this.onKeyInput.bind(this);
  }

  onKeyInput(e) {
    if (!e.ctrlKey && !e.shiftKey) {
      e.preventDefault();
      this.props.openSearch({ route: "knowledge.search" });
    }
  }

  registerPlugin() {
    Genesys("registerPlugin", this.sPluginName, (oPlugin) => {
      this.oKnowledge = oPlugin; //TODO: store plugin ref in Launcher and use that if already exists
      oPlugin.ready();
    });
  }

  componentDidMount() {
    this.registerPlugin();
  }

  render() {
    const { classes, i18n } = this.props;

    return (
      <Card>
        <List classes={{ root: classes.searchCard }}>
          <ListItem button classes={{ root: classes.searchCardItem }}>
            <div className={classes.searchLabel}>
              <Typography variant="h2">{i18n.SearchInputTitle}</Typography>
              <Icon
                color={"inherit"}
                aria-label="search"
                /* eslint-disable-next-line react/no-children-prop */
                children={<SearchIcon />}
                classes={{ root: classes.searchIcon }}
              />
            </div>
            <TextField
              id="searchBar"
              margin="dense"
              placeholder={i18n.SearchInputPlaceholder}
              variant="outlined"
              classes={{ root: classes.searchBar }}
              onKeyPress={this.onKeyInput}
              onClick={this.onKeyInput} // open the search view when user clicks on the search input in the card
            />
          </ListItem>
        </List>
      </Card>
    );
  }
}

Knowledge.propTypes = {
  openSearch: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired
};

const KnowledgeCard = withStyles(styles)(Knowledge);
const KnowledgeSearch = withStyles(styles)(Search);

export { KnowledgeCard, KnowledgeArticle, KnowledgeSearch, KnowledgeResults };
