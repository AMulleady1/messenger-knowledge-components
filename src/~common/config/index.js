import localConfig from "~common/config/local.config";
import devConfig from "~common/config/dev.config";
import prodConfig from "~common/config/prod.config";
import logger from "~common/utils/logger";

const configs = {
  local: localConfig,
  development: devConfig,
  production: prodConfig
};

const configDomainDefaults = {
  "127.0.0.1": "local",
  localhost: "local"
};

const hostName = window.location.hostname;
// eslint-disable-next-line no-undef
const nodeEnv = process.env.NODE_ENV;
const actualConfig = configDomainDefaults[hostName] || nodeEnv;

if (!actualConfig) {
  throw new Error(`Failed to load config based on NODE_ENV ${nodeEnv} under hostname ${hostName}`);
}

logger.log("Actual config: ", actualConfig);

export default configs[actualConfig];
