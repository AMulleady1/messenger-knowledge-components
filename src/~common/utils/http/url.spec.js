const TEST_RELATIVE_URL = "foo/{sessionId}/{orgId}/{kbId}/{articleId}";
const TEST_ID_NAME = "sessionId";
const TEST_ID_TO_REPLACE = "testId";

const TEST_IDS = {
  sessionId: "1234",
  orgId: "1234",
  kbId: "1234",
  articleId: "1234"
};

describe("url", () => {
  let url;

  beforeEach(async () => {
    jest.resetModules();
    const urlModule = await import("./url");
    url = urlModule.default;
  });

  describe("composeUrlChunk", () => {
    it("should return composed url chunk", () => {
      const { composeUrlChunk } = url;

      const replacedUrl = composeUrlChunk(TEST_RELATIVE_URL, TEST_ID_NAME, TEST_ID_TO_REPLACE);

      expect(replacedUrl).toContain("testId");
    });
    it("should throw error on invalid params", () => {
      const { composeUrlChunk } = url;
      let err;

      try {
        composeUrlChunk(TEST_RELATIVE_URL, TEST_ID_NAME, null);
      } catch (error) {
        err = error;
      }

      expect(err).toBeInstanceOf(Error);
    });
  });
  describe("composeUrl", () => {
    it("should return composed url", () => {
      const { composeUrl } = url;
      const expectedComposedUrl = "foo/1234/1234/1234/1234";

      const composedUrl = composeUrl(TEST_RELATIVE_URL, TEST_IDS);

      expect(composedUrl).toEqual(expectedComposedUrl);
    });
  });
});
