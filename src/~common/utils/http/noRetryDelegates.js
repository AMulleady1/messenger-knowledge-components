const noRetryDelegates = [];

const addNoRetryDelegates = (noRetryDelegate) => noRetryDelegates.push(noRetryDelegate);

const runNoRetryDelegates = (responseData) => {
  let retry = true;
  for (const noRetryDelegate of noRetryDelegates) {
    if (noRetryDelegate(responseData)) {
      retry = false;
    }
  }
  return retry;
};

const getNoRetryDelegates = () => JSON.parse(JSON.stringify(noRetryDelegates));

export default {
  addNoRetryDelegates,
  runNoRetryDelegates,
  getNoRetryDelegates
};
