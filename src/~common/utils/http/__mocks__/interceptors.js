const addBeforeRequestInterceptor = jest.fn();
const addAfterRequestInterceptor = jest.fn();
const runBeforeRequestInterceptors = jest.fn();
const runAfterRequestInterceptors = jest.fn();

export default {
  addBeforeRequestInterceptor,
  addAfterRequestInterceptor,
  runBeforeRequestInterceptors,
  runAfterRequestInterceptors
};
