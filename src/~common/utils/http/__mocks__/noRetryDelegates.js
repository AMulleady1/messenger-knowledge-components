const addNoRetryDelegates = jest.fn();
const runNoRetryDelegates = jest.fn();

export default {
  addNoRetryDelegates,
  runNoRetryDelegates
};
