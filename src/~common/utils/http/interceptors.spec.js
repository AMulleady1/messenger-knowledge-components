const TEST_TEXT = "TEST";
const TEST_INTERCEPTOR = (req = {}) => ({ ...req, test: TEST_TEXT });

describe("interceptors", () => {
  let interceptors;

  beforeEach(async () => {
    jest.resetModules();
    const interceptorModule = await import("./interceptors");
    interceptors = interceptorModule.default;
  });

  describe("getBeforeRequestInterceptors", () => {
    it("should return empty array", () => {
      const { getBeforeRequestInterceptors } = interceptors;

      const beforeInterceptors = getBeforeRequestInterceptors();

      expect(beforeInterceptors.length).toEqual(0);
    });
    it("should be immutable", () => {
      const { getBeforeRequestInterceptors } = interceptors;

      let beforeInterceptors = getBeforeRequestInterceptors();
      beforeInterceptors.push(TEST_INTERCEPTOR);
      beforeInterceptors = getBeforeRequestInterceptors();

      expect(beforeInterceptors.length).toEqual(0);
    });
  });
  describe("getAfterRequestInterceptors", () => {
    it("should return empty array", () => {
      const { getAfterRequestInterceptors } = interceptors;

      const afterInterceptors = getAfterRequestInterceptors();

      expect(afterInterceptors.length).toEqual(0);
    });
    it("should be immutable", () => {
      const { getAfterRequestInterceptors } = interceptors;

      let afterInterceptors = getAfterRequestInterceptors();
      afterInterceptors.push(TEST_INTERCEPTOR);
      afterInterceptors = getAfterRequestInterceptors();

      expect(afterInterceptors.length).toEqual(0);
    });
  });
  describe("addBeforeRequestInterceptor", () => {
    it("should add before request interceptor", () => {
      const { addBeforeRequestInterceptor, getBeforeRequestInterceptors } = interceptors;

      addBeforeRequestInterceptor(TEST_INTERCEPTOR);
      const beforeInterceptors = getBeforeRequestInterceptors();

      expect(beforeInterceptors.length).toEqual(1);
    });
  });
  describe("addAfterRequestInterceptor", () => {
    it("should add after request interceptor", () => {
      const { addAfterRequestInterceptor, getAfterRequestInterceptors } = interceptors;

      addAfterRequestInterceptor(TEST_INTERCEPTOR);
      const afterInterceptors = getAfterRequestInterceptors();

      expect(afterInterceptors.length).toEqual(1);
    });
  });
  describe("runBeforeRequestInterceptors", () => {
    it("should run before request interceptors", async () => {
      const { addBeforeRequestInterceptor, runBeforeRequestInterceptors } = interceptors;

      addBeforeRequestInterceptor(TEST_INTERCEPTOR);
      const updatedRequest = await runBeforeRequestInterceptors();

      expect(updatedRequest.test).toEqual(TEST_TEXT);
    });
  });
  describe("runAfterRequestInterceptors", () => {
    it("should run after request interceptors", async () => {
      const { addAfterRequestInterceptor, runAfterRequestInterceptors } = interceptors;

      addAfterRequestInterceptor(TEST_INTERCEPTOR);
      const updatedRequest = await runAfterRequestInterceptors();

      expect(updatedRequest.test).toEqual(TEST_TEXT);
    });
  });
});
