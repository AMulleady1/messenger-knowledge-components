const beforeRequestInterceptors = [];
const afterRequestInterceptors = [];

const getBeforeRequestInterceptors = () => JSON.parse(JSON.stringify(beforeRequestInterceptors));

const getAfterRequestInterceptors = () => JSON.parse(JSON.stringify(afterRequestInterceptors));

const addBeforeRequestInterceptor = (interceptor) => addInterceptor(interceptor, beforeRequestInterceptors);

const addAfterRequestInterceptor = (interceptor) => addInterceptor(interceptor, afterRequestInterceptors);

const addInterceptor = (interceptor, interceptors) => interceptors.push(interceptor);

const runBeforeRequestInterceptors = async (context) =>
  runInterceptors({ interceptors: beforeRequestInterceptors, context });

const runAfterRequestInterceptors = async (context) =>
  runInterceptors({ interceptors: afterRequestInterceptors, context });

const runInterceptors = async ({ interceptors, context }) => {
  let updatedContext = context;

  for (const interceptor of interceptors) {
    updatedContext = await interceptor(updatedContext);
  }

  return updatedContext;
};

export default {
  getBeforeRequestInterceptors,
  getAfterRequestInterceptors,
  addBeforeRequestInterceptor,
  addAfterRequestInterceptor,
  runBeforeRequestInterceptors,
  runAfterRequestInterceptors
};
