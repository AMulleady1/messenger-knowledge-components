const mockFn = jest.fn();

describe("retryDelegates", () => {
  let retryDelegates;

  beforeEach(async () => {
    jest.resetModules();
    const retryDelegateModule = await import("./noRetryDelegates");
    retryDelegates = retryDelegateModule.default;
  });

  describe("addNoRetryDelegates", () => {
    it("should add no retry delegates", () => {
      const { addNoRetryDelegates, getNoRetryDelegates } = retryDelegates;

      addNoRetryDelegates(mockFn);

      expect(getNoRetryDelegates().length).toEqual(1);
    });
  });
  describe("runNoRetryDelegates", () => {
    it("should run no retry delegates", () => {
      const { addNoRetryDelegates, runNoRetryDelegates } = retryDelegates;

      addNoRetryDelegates(mockFn);
      runNoRetryDelegates();

      expect(mockFn).toBeCalledTimes(1);
    });
  });
});
