const urlRestParams = ["sessionId", "orgId", "kbId", "articleId", "documentId"];

const composeUrl = (relativeUrl, ids) => {
  urlRestParams.forEach((urlRestParam) => {
    relativeUrl = composeUrlChunk(relativeUrl, urlRestParam, ids[urlRestParam]);
  });
  return relativeUrl;
};

const composeUrlChunk = (relativeUrl, idName, idToReplace) => {
  if (relativeUrl.indexOf(`{${idName}}`) !== -1 && !idToReplace) {
    throw new Error(`${idName} is not defined, however its required by the operation`);
  }
  return relativeUrl.replace(`{${idName}}`, idToReplace);
};

export default {
  composeUrl,
  composeUrlChunk
};
