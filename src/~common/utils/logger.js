import genesysService from "components/~common/genesys.srvc";

const log = (message, data) => {
  genesysService.log(message, data);
};

export default {
  log
};
