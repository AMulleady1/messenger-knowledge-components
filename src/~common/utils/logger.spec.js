import logger from "./logger";
import { log } from "components/~common/genesys.srvc";

jest.mock("components/~common/genesys.srvc");

const mockMessage = "Test";
const mockData = 1;

describe("logger", () => {
  beforeEach(async () => {
    jest.clearAllMocks();
  });
  describe("log", () => {
    it("should call publishMessage with message and data params", async () => {
      await logger.log(mockMessage, mockData);

      const logCalledFirstParam = log.mock.calls[0][0];
      const logCalledSecondParam = log.mock.calls[0][1];

      expect(logCalledFirstParam).toEqual(mockMessage);
      expect(logCalledSecondParam).toEqual(mockData);
    });
  });
});
