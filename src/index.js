import React from "react";
import ReactDOM from "react-dom";
import KnowledgeWrapper from "components/KnowledgeWrapper.jsx";

const targetDiv = document.getElementById("knowledge-app");

ReactDOM.render(<KnowledgeWrapper></KnowledgeWrapper>, targetDiv);
