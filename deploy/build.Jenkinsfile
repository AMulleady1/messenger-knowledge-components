#!/usr/bin/env groovy
@Library('pipeline-library@master')
@Library('bold360-pipeline-library@master')

// Constants
final String SNYK_ORG = 'octopus-0sw'
final String SNYK_SERVICE = 'messenger-knowledge-components'
final ArrayList<String> SNYK_TARGET_FILES = ['package.json', 'package-lock.json']
final String BUILD_IMAGE = 'zenika/alpine-chrome:89-with-puppeteer'

pipeline {
    agent {
        node {
            label 'dev_v2'
            customWorkspace "${JOB_NAME}-${currentBuild.number}"
        }
    }

    options {
        skipStagesAfterUnstable()
        buildDiscarder(logRotator(numToKeepStr: '10'))
        timeout(time: 1, unit: 'HOURS')
        timestamps()
        disableConcurrentBuilds()
    }

    environment {
        HOME = '/tmp'
        SNYK_TOKEN = credentials('PureCloudSnykToken')
    }

    tools {
        nodejs 'NodeJS 14.8.0'
    }

    stages {
        stage('Configure ECR') {
            steps {
                script {
                    awsUtils.loginEcr(awsUtils.DEFAULT_REGION, awsUtils.AWS_CREDENTIAL_ID, null)
                }
            }
        }

        stage('Initialize docker container') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }

            stages {
                stage('Install NPM packages') {
                    steps {
                        sh 'npm install'
                    }
                }

                stage('Lint') {
                    steps {
                        echo 'Lint should be run here...'
                    }
                }

                stage('Build application') {
                    steps {
                        sh 'npm run build'
                    }
                }

                stage('Test') {
                    steps {
                        script {
                            sh 'npm run test'
                        }
                    }
                    post {
                        always {
                            junit 'coverage/junit/junit.xml'
                            step([$class: 'CoberturaPublisher', autoUpdateHealth: false, autoUpdateStability: false,  coberturaReportFile: 'coverage/jest/cobertura-coverage.xml', lineCoverageTargets: '0, 0, 0', failUnstable: false,  onlyStable: false])
                        }
                    }
                }
            }
        }

        stage('Codacy coverage report') {
            steps {
                script {
                    codacyUtils.uploadCoberturaCoverage('927e6f400ba944d79f0c9dac281d2380');
                }
            }
        }

        stage('Snyk') {
            steps {
                script {
                    if (env.BRANCH_NAME == 'master') {
                        snykUtils.monitor(SNYK_SERVICE, SNYK_TARGET_FILES, '', SNYK_ORG)
                    }
                    snykUtils.test(SNYK_SERVICE, SNYK_TARGET_FILES, '--severity-threshold=high', SNYK_ORG)
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
