#!/usr/bin/env groovy
@Library('pipeline-library@master')
@Library('bold360-pipeline-library@master')

final String CONFIRMATION = 'Do you really want to make a RELEASE version?'
final String BUILD_IMAGE = "zenika/alpine-chrome:89-with-puppeteer"

pipeline {
    agent {
        node {
            label 'dev_v2'
            customWorkspace "${JOB_NAME}-${currentBuild.number}"
        }
    }

    parameters {
        booleanParam(name: CONFIRMATION, defaultValue: false, description: 'Required confirmation checkbox')
    }

    options {
        skipStagesAfterUnstable()
        buildDiscarder(logRotator(numToKeepStr: '10'))
        timeout(time: 1, unit: 'HOURS')
        timestamps()
        disableConcurrentBuilds()
    }

    environment {
        HOME = '/tmp'
    }

    stages {
        stage('Initialize') {
            steps {
                script {
                    jenkinsUtils.abortOnAutoBuild()

                    if (!isMasterBranch()) {
                        stopWithError('This pipeline is allowed only on MASTER branch')
                    }

                    if (!jenkinsUtils.isEnabled(CONFIRMATION)) {
                        stopWithError('Confirmation parameter is required')
                    }
                }
            }
        }

        stage('Configure ECR') {
            steps {
                script {
                    awsUtils.loginEcr(awsUtils.DEFAULT_REGION, awsUtils.AWS_CREDENTIAL_ID, null)
                }
            }
        }

        stage('Install NPM packages') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                sh "npm install"
            }
        }

        stage('Lint') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                echo "Lint should be run here..."
            }
        }

        stage('Build application') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                sh "npm run build"
            }
        }

        stage('Test') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                echo "Test should be run here..."
            }
        }

        stage('Configure NPM registry') {
            steps {
                configureNpm()
            }
        }

        stage('Publish to NPM registry') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                 sh "npm publish"
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}

boolean isMasterBranch() {
    return 'master'.equalsIgnoreCase(env.BRANCH_NAME)
}

def stopWithError(String errorMessage = '') {
    currentBuild.result = 'FAILURE'
    error(errorMessage)
}
