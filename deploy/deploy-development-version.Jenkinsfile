#!/usr/bin/env groovy
@Library('pipeline-library@master')
@Library('bold360-pipeline-library@master')

final String BUILD_IMAGE = "zenika/alpine-chrome:89-with-puppeteer"

pipeline {
    agent {
        node {
            label 'dev_v2'
            customWorkspace "${JOB_NAME}-${currentBuild.number}"
        }
    }

    options {
        skipStagesAfterUnstable()
        buildDiscarder(logRotator(numToKeepStr: '10'))
        timeout(time: 1, unit: 'HOURS')
        timestamps()
        disableConcurrentBuilds()
    }

    environment {
        HOME = '/tmp'
    }

    stages {
        stage('Initialize') {
            steps {
                script {
                    jenkinsUtils.abortOnAutoBuild()

                    if (!isBranchTypeEnabled()) {
                        stopWithError('This pipeline is not allowed on this branch')
                    }
                }
            }
        }

        stage('Configure ECR') {
            steps {
                script {
                    awsUtils.loginEcr(awsUtils.DEFAULT_REGION, awsUtils.AWS_CREDENTIAL_ID, null)
                }
            }
        }

        stage('Install NPM packages') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                sh "npm install"
            }
        }

        stage('Lint') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                echo "Lint should be run here..."
            }
        }

        stage('Build application') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                sh "npm run build"
            }
        }

        stage('Test') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                echo "Test should be run here..."
            }
        }

        stage('Set package version tag') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                sh "npm version prerelease --preid ${getPrereleasePrefix()} -m \"DEVELOPMENT version deploy, tag version\" --no-commit-hooks -f"
            }
        }

        stage('Add version tag to git') {
            steps {
                script {
                    pushTag()
                }
            }
        }

        stage('Configure NPM registry') {
            steps {
                configureNpm()
            }
        }

        stage('Publish to NPM registry') {
            agent {
                docker {
                    registryUrl awsUtils.getEcrRepository()
                    image BUILD_IMAGE
                    reuseNode true
                }
            }
            steps {
                 sh "npm publish"
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}

boolean isBranchTypeEnabled() {
    return jenkinsUtils.isDevelopBranch() || isFeatureBranch() || isBugfixBranch() || jenkinsUtils.isReleaseBranch() || jenkinsUtils.isHotfixBranch()
}

boolean isFeatureBranch() {
    return env.BRANCH_NAME.toLowerCase().startsWith('feature/')
}

boolean isBugfixBranch() {
    return env.BRANCH_NAME.toLowerCase().startsWith('bugfix/')
}

String getJiraTicketFromBranchName() {
    def matcher = (env.BRANCH_NAME =~ '/([a-zA-Z]+-[0-9]+).*')
    return matcher[0][1]
}

String getVersionFromBranchName() {
    def matcher = (env.BRANCH_NAME =~ 'release/(.+)')
    return matcher[0][1]
}

String getPrereleasePrefix() {
    if (isFeatureBranch() || isBugfixBranch() || jenkinsUtils.isHotfixBranch()) {
        return getJiraTicketFromBranchName()
    }
    else if (jenkinsUtils.isReleaseBranch()) {
        return getVersionFromBranchName()
    }
    else {
        return "develop"
    }
}

def pushTag(String credentialId = constants.credentials.bitbucket.jenkins) {
    final String gitHttpUrl = sh(script: 'git remote get-url origin', returnStdout: true).trim()
    final String gitSshUrl = gitHttpUrl.replace('https://bitbucket.org/', 'git@bitbucket.org:')
    sh("git remote set-url origin ${gitSshUrl} ${gitHttpUrl}")

    try {
        sshagent(credentials: [credentialId]) {
            sh("git config user.email '${constants.git_config['email']}'")
            sh("git config user.name '${constants.git_config['username']}'")
            sh("git push --follow-tags --set-upstream origin ${env.BRANCH_NAME}")
        }
    } finally {
        sh("git remote set-url origin ${gitHttpUrl} ${gitSshUrl}")
    }
}

def stopWithError(String errorMessage = '') {
    currentBuild.result = 'FAILURE'
    error(errorMessage)
}
