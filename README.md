[![Codacy Badge](https://app.codacy.com/project/badge/Grade/7fdc995f1af445b5b6fa67108f4beb40)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=inindca/messenger-knowledge-components&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/7fdc995f1af445b5b6fa67108f4beb40)](https://www.codacy.com?utm_source=bitbucket.org&utm_medium=referral&utm_content=inindca/messenger-knowledge-components&utm_campaign=Badge_Coverage)

# README

Generates Knowledge React components for usage in Messenger UI. These are published to Genesys Cloud NPM repository, Messenger will include this in the build as seperate JS assests and lazy load them as needed.

### What is this repository for?

- Knowledge React components for Messenger Client
- [Technical Design](https://intranet.genesys.com/display/GeneWid/Knowledge+App+from+NPM)

### How do I get set up?

- clone repo
- install node version 10 or above
- run: npm install
- Note: On Windows machines please first run npm install -g win-node-env
- run: 'npm run dev' for dev build
- run: 'npm run build' for production build
- run: 'npm run pack' to generate an npm package. This will reside in your local file system.
- Note: In the .gitignore file delete the line `build/` when using the local npm file to install this package in Messenger. Rever this file back before committing the changes.

### Who do I talk to?

- Ranjith Manikante Sai / Benjamin Friend
- OctopusTeam <bold360octopus@genesys.com>
